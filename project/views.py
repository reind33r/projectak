from django.shortcuts import render, get_object_or_404, redirect, reverse
from django.views.generic.edit import UpdateView, CreateView, DeleteView, FormView
from django.views.generic.detail import DetailView
from django.views.generic.list import ListView
from projectak.decorators.permission_required import permission_required

class ProjectViewBase:
    def get_project(self):
        project_pk = self.kwargs.get('project_pk', False)
        from .models import Project
        return get_object_or_404(Project, pk=project_pk)

    def get_context_data(self, **kwargs):
        context = super(ProjectViewBase, self).get_context_data(**kwargs)

        context['project'] = self.get_project()

        return context

class ProjectFormViewBase:
    def get_context_data(self, **kwargs):
        context = super(ProjectFormViewBase, self).get_context_data(**kwargs)

        context['form'].fields['manager'].queryset = self.request.user.contact.organization.get_members()
        context['form'].fields['members'].queryset = self.request.user.contact.organization.get_members()

        return context

class TaskFormViewBase:
    def get_context_data(self, **kwargs):
        context = super(TaskFormViewBase, self).get_context_data(**kwargs)

        context['form'].fields['members'].queryset = self.get_project().members

        return context

class TaskListViewBase(ProjectViewBase):
    def get_task_list(self):
        task_list_pk = self.kwargs.get('task_list_pk', False)
        from .models import TaskList
        return get_object_or_404(TaskList, pk=task_list_pk)

    def get_context_data(self, **kwargs):
        context = super(TaskListViewBase, self).get_context_data(**kwargs)

        context['task_list'] = self.get_task_list()

        return context


# Project
class ProjectListView(ListView):
    from .models import Project
    model = Project
    context_object_name = 'projects'

    template_name = "project/project/list.html"

    def get_context_data(self, **kwargs):
        context = super(ProjectListView, self).get_context_data(**kwargs)

        projects = self.get_queryset()
        context['active_projects'] = projects.filter(closed=False)
        context['closed_projects'] = projects.filter(closed=True)

        return context

    def get_queryset(self):
        return self.request.user.contact.get_projects()

@permission_required('organization.add_project_organization')
class ProjectAddView(ProjectFormViewBase, CreateView):
    from .models import Project
    model = Project
    from .forms import ProjectAddForm
    form_class = ProjectAddForm

    template_name = "project/project/add.html"

    def form_valid(self, form):
        project = form.save(commit=False)
        project.organization = self.request.user.contact.organization

        return super(ProjectAddView, self).form_valid(form)

@permission_required('project.view_project')
class ProjectOverviewView(ProjectViewBase, DetailView):
    pk_url_kwarg = "project_pk"
    from .models import Project
    model=Project

    template_name = "project/project/overview.html"
    context_object_name = "project"

    def get_context_data(self, **kwargs):
        context = super(ProjectOverviewView, self).get_context_data(**kwargs)

        from .models import Task, order_queryset
        context['tasksForToday'] = order_queryset(Task.objects.filter(closed=False, task_list__project=self.get_project(), members__in=[self.request.user.contact.pk]))[:5]

        return context

@permission_required('project.view_project')
class ProjectHistoryView(ProjectViewBase, DetailView):
    pk_url_kwarg = "project_pk"
    from .models import Project
    model=Project

    template_name = "project/project/history.html"
    context_object_name = "project"

    def get_context_data(self, **kwargs):
        from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger

        context = super(ProjectHistoryView, self).get_context_data(**kwargs)

        entries = self.get_project().history_set.all()

        paginator = Paginator(entries, 25)
        page = self.request.GET.get('page')
        try:
            entries = paginator.page(page)
        except PageNotAnInteger:
            entries = paginator.page(1)
        except EmptyPage:
            entries = paginator.page(paginator.num_pages)


        context['entries'] = entries

        return context

@permission_required('project.change_project')
class ProjectEditView(ProjectFormViewBase, ProjectViewBase, UpdateView):
    pk_url_kwarg = "project_pk"

    from .models import Project
    model = Project
    from .forms import ProjectEditForm
    form_class = ProjectEditForm

    template_name = "project/project/edit.html"
    context_object_name = "project"

@permission_required('project.delete_project')
class ProjectDeleteView(ProjectViewBase, DeleteView):
    pk_url_kwarg = "project_pk"

    from .models import Project
    model = Project

    template_name = "project/project/delete.html"
    context_object_name = "project"

    def get_success_url(self):
        from django.core.urlresolvers import reverse_lazy
        return reverse_lazy('projects_list')

from django.views.generic import View
from django.views.generic.detail import SingleObjectMixin
@permission_required('project.close_project')
class ProjectCloseView(ProjectViewBase, SingleObjectMixin, View):
    pk_url_kwarg = "project_pk"
    from .models import Project
    model=Project

    def get(self, request, *args, **kwargs):
        project = self.get_object()

        project.closed = not project.closed

        if project.closed:
            from datetime import date
            project.closed_date = date.today()
        else:
            project.closed_date = None

        project.save()

        return redirect('project_overview', project_pk=self.get_project().pk)

# Tasks lists
@permission_required('project.view_project')
class TaskListListView(ProjectViewBase, ListView):
    from .models import TaskList
    model = TaskList

    template_name = "project/task_lists/list.html"

    def get_queryset(self):
        from .models import TaskList, order_queryset
        return order_queryset(TaskList.objects.filter(project=self.get_project()))

@permission_required('project.add_task_list_project')
class TaskListAddView(ProjectViewBase, CreateView):
    from .models import TaskList
    model = TaskList
    from .forms import TaskListAddForm
    form_class = TaskListAddForm

    template_name = "project/task_lists/add.html"

    def get_context_data(self, **kwargs):
        context = super(TaskListAddView, self).get_context_data(**kwargs)

        context['form'].fields['milestone'].queryset = self.get_project().milestone_set.all()

        for task_form in context['form'].formsets['tasks']:
            task_form.fields['members'].queryset = self.get_project().members

        if self.get_milestone():
            context['milestone'] = self.get_milestone()

        return context

    def get_milestone(self):
        milestone_pk = self.kwargs.get('milestone_pk', None)
        if milestone_pk is not None:
            from .models import Milestone
            milestone = get_object_or_404(Milestone, pk=milestone_pk)
            return milestone
        return False


    def form_valid(self, form):
        task_list = form.save(commit=False)
        task_list.project = self.get_project()

        return super(TaskListAddView, self).form_valid(form)

    def get_initial(self):
        if self.get_milestone():
            return { 'milestone': self.get_milestone() }

        return {}

@permission_required('project.view_tasklist')
class TaskListDetailView(ProjectViewBase, DetailView):
    pk_url_kwarg = "task_list_pk"
    from .models import TaskList
    model=TaskList

    template_name = "project/task_lists/view.html"
    context_object_name = "task_list"

@permission_required('project.change_tasklist')
class TaskListEditView(ProjectViewBase, UpdateView):
    pk_url_kwarg = "task_list_pk"

    from .models import TaskList
    model = TaskList
    from .forms import TaskListForm
    form_class = TaskListForm

    template_name = "project/task_lists/edit.html"
    context_object_name = "task_list"

    def get_context_data(self, **kwargs):
        context = super(TaskListEditView, self).get_context_data(**kwargs)

        context['form'].fields['milestone'].queryset = self.get_project().milestone_set.all()

        return context

@permission_required('project.delete_tasklist')
class TaskListDeleteView(ProjectViewBase, DeleteView):
    pk_url_kwarg = "task_list_pk"

    from .models import TaskList
    model = TaskList

    template_name = "project/task_lists/delete.html"
    context_object_name = "task_list"

    def get_success_url(self):
        from django.core.urlresolvers import reverse_lazy
        return reverse_lazy('project_task_lists_list', kwargs={ 'project_pk': self.kwargs.get('project_pk') })


class TaskListCopyView(ProjectViewBase, UpdateView):
    pk_url_kwarg = "task_list_pk"

    from .models import TaskList
    model = TaskList

    from .forms import TaskListCopyForm
    form_class = TaskListCopyForm

    template_name = "project/task_lists/copy.html"
    context_object_name = "task_list"

    def get_initial(self):
        return { 'keep_pinned_users': False, 'reset_tasks_status': True, 'reset_tasks_due_date': True, 'reset_tasks_priority': False }

    def get_context_data(self, **kwargs):
        context = super(TaskListCopyView, self).get_context_data(**kwargs)

        from .models import Project

        if self.request.user.contact == self.request.user.contact.organization.manager:
            projects = Project.objects.filter(organization=self.request.user.contact.organization)
        else:
            projects = Project.objects.filter(organization=self.request.user.contact.organization, manager=self.request.user.contact)

        context['form'].fields['destination_project'].queryset = projects

        return context

    def form_valid(self, form):
        task_list = self.get_object()

        originalTasks = task_list.task_set.all()
        originalMilestone = task_list.milestone
        originalProject = task_list.project

        task_list.pk = None # we "create a copy"
        task_list.project = form.cleaned_data['destination_project']

        if originalProject == form.cleaned_data['destination_project'] and form.cleaned_data['keep_milestone']:
            task_list.milestone = originalMilestone

        if form.cleaned_data['reset_tasks_status']:
            task_list.closed = False
            task_list.closed_date = None

        task_list.name = form.cleaned_data['name']
        task_list.description = form.cleaned_data['description']
        task_list.due_date = form.cleaned_data['due_date']
        task_list.priority = form.cleaned_data['priority']

        task_list.save()

        for task in originalTasks:
            originalMembers = task.members.all()

            task.pk = None
            task.task_list = task_list

            if form.cleaned_data['reset_tasks_status']:
                task.closed = False
                task.closedDate = None

            if form.cleaned_data['reset_tasks_due_date']:
                task.due_date = None

            if form.cleaned_data['reset_tasks_priority']:
                task.priority = None

            task.save()

            if form.cleaned_data['keep_pinned_users']:
                for member in originalMembers:
                    if member in task_list.project.members.all():
                        task.members.add(member)

        return redirect(task_list.get_absolute_url())

class TaskListMoveView(ProjectViewBase, UpdateView):
    pk_url_kwarg = "task_list_pk"

    from .models import TaskList
    model = TaskList

    from .forms import TaskListMoveForm
    form_class = TaskListMoveForm

    template_name = "project/task_lists/move.html"
    context_object_name = "task_list"

    def get_context_data(self, **kwargs):
        context = super(TaskListMoveView, self).get_context_data(**kwargs)

        from .models import Project

        if self.request.user.contact == self.request.user.contact.organization.manager:
            projects = Project.objects.filter(organization=self.request.user.contact.organization)
        else:
            projects = Project.objects.filter(organization=self.request.user.contact.organization, manager=self.request.user.contact)

        projects = projects.exclude(pk=self.get_project().pk)
        context['form'].fields['project'].queryset = projects

        return context

    def get_initial(self):
        return { 'project': None, 'keep_pinned_users': True, 'reset_tasks_status': False, 'reset_tasks_due_date': False, 'reset_tasks_priority': False }

    def form_valid(self, form):
        task_list = self.get_object()

        if form.cleaned_data['reset_tasks_status']:
            task_list.closed = False
            task_list.closed_date = None

        for task in task_list.task_set.all():
            if form.cleaned_data['reset_tasks_status']:
                task.closed = False
                task.closedDate = None

            if form.cleaned_data['reset_tasks_due_date']:
                task.due_date = None

            if form.cleaned_data['reset_tasks_priority']:
                task.priority = None

            task.save()

            for member in task.members.all():
                if not form.cleaned_data['keep_pinned_users'] or not member in form.cleaned_data['project'].members.all():
                    task.members.remove(member)

        return super(TaskListMoveView, self).form_valid(form)

@permission_required('project.view_project')
class TaskListHistoryView(ProjectViewBase, DetailView):
    pk_url_kwarg = "task_list_pk"
    from .models import TaskList
    model=TaskList

    template_name = "project/task_lists/history.html"
    context_object_name = "task_list"

    def get_context_data(self, **kwargs):
        from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger

        context = super(TaskListHistoryView, self).get_context_data(**kwargs)

        entries = self.get_object().history_set.all()

        paginator = Paginator(entries, 25)
        page = self.request.GET.get('page')
        try:
            entries = paginator.page(page)
        except PageNotAnInteger:
            entries = paginator.page(1)
        except EmptyPage:
            entries = paginator.page(paginator.num_pages)


        context['entries'] = entries

        return context


# Tasks
@permission_required('project.add_task_project')
class TaskAddView(TaskListViewBase, TaskFormViewBase, CreateView):
    from .models import Task
    model = Task
    from .forms import TaskForm
    form_class = TaskForm

    template_name = "project/tasks/add.html"

    def form_valid(self, form):
        task = form.save(commit=False)
        task.task_list = self.get_task_list()

        return super(TaskAddView, self).form_valid(form)

@permission_required('project.change_task')
class TaskEditView(TaskListViewBase, TaskFormViewBase, UpdateView):
    pk_url_kwarg = "task_pk"

    from .models import Task
    model = Task
    from .forms import TaskForm
    form_class = TaskForm

    template_name = "project/tasks/edit.html"
    context_object_name = "task"

@permission_required('project.delete_task')
class TaskDeleteView(TaskListViewBase, DeleteView):
    pk_url_kwarg = "task_pk"

    from .models import Task
    model = Task

    template_name = "project/tasks/delete.html"
    context_object_name = "task"

    def get_success_url(self):
        from django.core.urlresolvers import reverse_lazy
        return reverse_lazy('project_task_lists_view', kwargs={ 'project_pk': self.kwargs.get('project_pk'), 'task_list_pk': self.kwargs.get('task_list_pk') })


from django.views.generic import View
from django.views.generic.detail import SingleObjectMixin
@permission_required('project.close_task')
class TaskCloseView(TaskListViewBase, SingleObjectMixin, View):
    pk_url_kwarg = "task_pk"
    from .models import Task
    model=Task

    def get(self, request, *args, **kwargs):
        task = self.get_object()

        task.closed = not task.closed

        if task.closed:
            from datetime import date
            task.closed_date = date.today()
        else:
            task.closed_date = None

        task.save()

        quick = self.kwargs.get('quick', False)
        if quick:
            milestone_pk = self.kwargs.get('milestone_pk', False)
            from django.core.urlresolvers import reverse

            if milestone_pk is not False:
                return redirect('{}#tasklist_{}'.format(reverse('project_milestones_view', kwargs={'project_pk':self.get_project().pk, 'milestone_pk': milestone_pk}), task.task_list.pk))
            else:
                return redirect('{}#tasklist_{}'.format(reverse('project_task_lists_list', kwargs={'project_pk':self.get_project().pk}), task.task_list.pk))

        else:
            return redirect('project_task_lists_view', project_pk=self.get_project().pk, task_list_pk=task.task_list.pk)


# Milestones
@permission_required('project.add_milestone_project')
class MilestoneAddView(ProjectViewBase, CreateView):
    from .models import Milestone
    model = Milestone
    from .forms import MilestoneForm
    form_class = MilestoneForm

    template_name = "project/milestones/add.html"

    def form_valid(self, form):
        milestone = form.save(commit=False)
        milestone.project = self.get_project()

        return super(MilestoneAddView, self).form_valid(form)

@permission_required('project.view_project')
class MilestoneListView(ProjectViewBase, ListView):
    from .models import Milestone
    model = Milestone

    template_name = "project/milestones/list.html"

    def get_queryset(self):
        from .models import Milestone
        return Milestone.objects.filter(project=self.get_project())

@permission_required('project.view_milestone')
class MilestoneDetailView(ProjectViewBase, DetailView):
    pk_url_kwarg = "milestone_pk"
    from .models import Milestone
    model=Milestone

    template_name = "project/milestones/view.html"
    context_object_name = "milestone"

@permission_required('project.change_milestone')
class MilestoneEditView(ProjectViewBase, UpdateView):
    pk_url_kwarg = "milestone_pk"

    from .models import Milestone
    model = Milestone
    from .forms import MilestoneForm
    form_class = MilestoneForm

    template_name = "project/milestones/edit.html"
    context_object_name = "milestone"

@permission_required('project.delete_milestone')
class MilestoneDeleteView(ProjectViewBase, DeleteView):
    pk_url_kwarg = "milestone_pk"

    from .models import Milestone
    model = Milestone

    template_name = "project/milestones/delete.html"
    context_object_name = "milestone"

    def get_success_url(self):
        from django.core.urlresolvers import reverse_lazy
        return reverse_lazy('project_milestones_list', kwargs={ 'project_pk': self.kwargs.get('project_pk') })

from django.views.generic import View
from django.views.generic.detail import SingleObjectMixin
@permission_required('project.close_milestone')
class MilestoneCloseView(ProjectViewBase, SingleObjectMixin, View):
    pk_url_kwarg = "milestone_pk"
    from .models import Milestone
    model=Milestone

    def get(self, request, *args, **kwargs):
        milestone = self.get_object()

        milestone.closed = not milestone.closed

        if milestone.closed:
            from datetime import date
            milestone.closed_date = date.today()
        else:
            milestone.closed_date = None

        milestone.save()

        quick = self.kwargs.get('quick', False)
        if quick:
            return redirect('project_milestones_list', project_pk=self.get_project().pk)
        else:
            return redirect('project_milestones_view', project_pk=self.get_project().pk, milestone_pk=milestone.pk)

@permission_required('project.view_project')
class MilestoneHistoryView(ProjectViewBase, DetailView):
    pk_url_kwarg = "milestone_pk"
    from .models import Milestone
    model=Milestone

    template_name = "project/milestones/history.html"
    context_object_name = "milestone"

    def get_context_data(self, **kwargs):
        from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger

        context = super(MilestoneHistoryView, self).get_context_data(**kwargs)

        entries = self.get_object().history_set.all()

        paginator = Paginator(entries, 25)
        page = self.request.GET.get('page')
        try:
            entries = paginator.page(page)
        except PageNotAnInteger:
            entries = paginator.page(1)
        except EmptyPage:
            entries = paginator.page(paginator.num_pages)


        context['entries'] = entries

        return context


# Messages
class MessagePostView(ProjectViewBase, CreateView):
    from .models import Message
    model = Message
    from .forms import MessageForm
    form_class = MessageForm

    template_name = "project/messages/post.html"

    def form_valid(self, form):
        form.instance.project = self.get_project()
        form.instance.author = self.request.user.contact

        return super(MessagePostView, self).form_valid(form)

    def get_success_url(self):
        from django.core.urlresolvers import reverse_lazy
        return reverse_lazy('project_messages_list', kwargs={"project_pk": self.get_project().pk })

@permission_required('project.change_message')
class MessageEditView(ProjectViewBase, UpdateView):
    pk_url_kwarg = "message_pk"

    from .models import Message
    model = Message
    from .forms import MessageForm
    form_class = MessageForm

    template_name = "project/messages/edit.html"
    context_object_name = "message"

@permission_required('project.delete_message')
class MessageDeleteView(ProjectViewBase, DeleteView):
    pk_url_kwarg = "message_pk"

    from .models import Message
    model = Message

    template_name = "project/messages/delete.html"
    context_object_name = "message"

    def get_success_url(self):
        from django.core.urlresolvers import reverse_lazy
        return reverse_lazy('project_messages_list', kwargs={"project_pk": self.get_project().pk })


class MessageListView(ProjectViewBase, ListView):
    from .models import Message
    model = Message
    context_object_name = 'messages'

    template_name = "project/messages/list.html"


    def get_context_data(self, **kwargs):
        from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger

        context = super(MessageListView, self).get_context_data(**kwargs)

        messages = self.get_queryset()

        paginator = Paginator(messages, 5)
        page = self.request.GET.get('page')
        try:
            messages = paginator.page(page)
        except PageNotAnInteger:
            messages = paginator.page(1)
        except EmptyPage:
            messages = paginator.page(paginator.num_pages)

        context['messages'] = messages

        return context


    def get_queryset(self):
        return self.get_project().message_set.all()

from .forms import CommentForm
class MessageDetailView(ProjectViewBase, DetailView):
    pk_url_kwarg = "message_pk"
    from .models import Message
    model=Message

    template_name = "project/messages/view.html"
    context_object_name = "message"


    def get_context_data(self, **kwargs):
        from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger

        context = super(MessageDetailView, self).get_context_data(**kwargs)

        comments = self.get_object().comment_set.all()

        paginator = Paginator(comments, 10)
        page = self.request.GET.get('page')
        try:
            comments = paginator.page(page)
        except PageNotAnInteger:
            comments = paginator.page(1)
        except EmptyPage:
            comments = paginator.page(paginator.num_pages)

        context['comments'] = comments
        context['form_quick_answer'] = CommentForm()

        return context

    def post(self, request, *args, **kwargs):
        if not self.get_object().allow_comments:
            from django.core.exceptions import PermissionDenied
            raise PermissionDenied

        form = CommentForm(request.POST)
        if form.is_valid():
            comment = form.save(commit=False)

            comment.message = self.get_object()

            comment.author = request.user.contact
            import datetime
            comment.date = datetime.datetime.now()

            comment.save()

            return redirect('{}?page={}#comment-{}'.format(reverse('project_messages_view', kwargs={'project_pk': self.get_project().pk, 'message_pk':self.get_object().pk}), comment.message.get_comment_page(comment), comment.pk))

        return render(request, self.template_name, {'form_quick_answer': form})

# will return 404 if not orgnization member because of project__pk
def download_message_attachment(request, project_pk, message_pk, attachment_pk):
    from .models import Attachment
    attachment = get_object_or_404(Attachment, pk=attachment_pk, message__pk=message_pk, message__project__pk=project_pk)

    from sendfile import sendfile
    return sendfile(request, attachment.file.path, attachment=True)


from django.views.generic import View
from django.views.generic.detail import SingleObjectMixin
@permission_required('project.stick_message_project')
class MessageStickView(ProjectViewBase, SingleObjectMixin, View):
    pk_url_kwarg = "message_pk"
    from .models import Message
    model=Message

    def get(self, request, *args, **kwargs):
        message = self.get_object()

        message.sticky = not message.sticky

        message.save()

        message_pk = self.kwargs.get('message_pk', False)

        return redirect('project_messages_list', project_pk=self.get_project().pk)


@permission_required('project.change_comment')
class CommentEditView(ProjectViewBase, UpdateView):
    pk_url_kwarg = "comment_pk"

    from .models import Comment
    model = Comment
    from .forms import CommentForm
    form_class = CommentForm

    template_name = "project/messages/comment_edit.html"
    context_object_name = "comment"

@permission_required('project.delete_comment')
class CommentDeleteView(ProjectViewBase, DeleteView):
    pk_url_kwarg = "comment_pk"

    from .models import Comment
    model = Comment

    template_name = "project/messages/comment_delete.html"
    context_object_name = "comment"

    def get_success_url(self):
        from django.core.urlresolvers import reverse_lazy
        return reverse_lazy('project_messages_view', kwargs={'project_pk': self.get_project().pk, 'message_pk': self.get_object().message.pk })
