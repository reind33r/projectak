from permission.logics.base import PermissionLogic

PERMISSION_DEFAULT_FIELD_NAME = 'user'
PERMISSION_DEFAULT_ANY_PERMISSION = False
PERMISSION_DEFAULT_VIEW_PERMISSION = False
PERMISSION_DEFAULT_CHANGE_PERMISSION = False
PERMISSION_DEFAULT_DELETE_PERMISSION = False
PERMISSION_DEFAULT_CLOSE_PERMISSION = False
PERMISSION_DEFAULT_VIEW_MEMBERS_PERMISSION = False

from abc import ABCMeta, abstractmethod
class BasePermissionLogic(PermissionLogic, metaclass=ABCMeta):
    def __init__(self,
                 field_name=None,
                 any_permission=None,
                 view_permission=None,
                 change_permission=None,
                 delete_permission=None,
                 close_permission=None,
                 view_members_permission=None):

        self.field_name = field_name

        self.any_permission = any_permission
        self.view_permission = view_permission
        self.change_permission = change_permission
        self.delete_permission = delete_permission
        self.close_permission = close_permission
        self.view_members_permission = view_members_permission

        if self.any_permission is None:
            self.any_permission = PERMISSION_DEFAULT_ANY_PERMISSION

        if self.view_permission is None:
            self.view_permission = PERMISSION_DEFAULT_VIEW_PERMISSION

        if self.change_permission is None:
            self.change_permission = PERMISSION_DEFAULT_CHANGE_PERMISSION

        if self.delete_permission is None:
            self.delete_permission = PERMISSION_DEFAULT_DELETE_PERMISSION

        if self.close_permission is None:
            self.close_permission = PERMISSION_DEFAULT_CLOSE_PERMISSION

        if self.view_members_permission is None:
            self.view_members_permission = PERMISSION_DEFAULT_VIEW_MEMBERS_PERMISSION

    @abstractmethod
    def _has_perm_user_field_check(self, user_obj, obj):
        pass

    def has_perm(self, user_obj, perm, obj=None):
        if not user_obj.is_authenticated():
            return False

        view_permission = self.get_full_permission_string('view')
        change_permission = self.get_full_permission_string('change')
        delete_permission = self.get_full_permission_string('delete')

        close_permission = self.get_full_permission_string('close')
        view_members_permission = self.get_full_permission_string('view_members')

        if obj is None or self._has_perm_user_field_check(user_obj, obj):
            if self.any_permission:
                return True

            elif perm == view_permission:
                return self.view_permission

            elif perm == change_permission:
                return self.change_permission

            elif perm == delete_permission:
                return self.delete_permission

            elif perm == close_permission:
                return self.close_permission

            elif perm == view_members_permission:
                return self.view_members_permission

        return False


class SingleUserPermissionLogic(BasePermissionLogic):
    def _has_perm_user_field_check(self, user_obj, obj):
        from permission.utils.field_lookup import field_lookup
        user_from_obj = field_lookup(obj, self.field_name)
        if user_from_obj == user_obj:
            return True

        return False


class ManyUsersPermissionLogic(BasePermissionLogic):
    def _has_perm_user_field_check(self, user_obj, obj):
        from permission.utils.field_lookup import field_lookup
        users_from_obj = field_lookup(obj, self.field_name)

        if hasattr(users_from_obj, 'all'):
            users_from_obj = users_from_obj.all()

        if user_obj in users_from_obj:
            return True

        return False