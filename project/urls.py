from django.conf.urls import url, include

from .views import (ProjectListView, ProjectAddView, ProjectOverviewView, ProjectEditView, ProjectDeleteView, ProjectCloseView, ProjectHistoryView,
    TaskListListView, TaskListAddView, TaskListDetailView, TaskListEditView, TaskListDeleteView, TaskListCopyView, TaskListMoveView, TaskListHistoryView,
    TaskEditView, TaskDeleteView, TaskAddView, TaskCloseView,
    MilestoneListView, MilestoneAddView, MilestoneDetailView, MilestoneEditView, MilestoneDeleteView, MilestoneCloseView, MilestoneHistoryView,
    MessageListView, MessagePostView, MessageDetailView, MessageEditView, MessageDeleteView, MessageStickView, CommentEditView, CommentDeleteView, download_message_attachment)

urlpatterns = [
    url(r'^$', ProjectListView.as_view(), name='projects_list'),
    url(r'^add/$', ProjectAddView.as_view(), name='project_add'),
    url(r'^(?P<project_pk>\d+)/', include([
        url(r'^$', ProjectOverviewView.as_view(), name='project_overview'),
        url(r'^edit/$', ProjectEditView.as_view(), name='project_edit'),
        url(r'^delete/$', ProjectDeleteView.as_view(), name='project_delete'),
        url(r'^check/$', ProjectCloseView.as_view(), name='project_check'),
        url(r'^history/$', ProjectHistoryView.as_view(), name='project_history'),
        url(r'^milestones/', include([
            url(r'^$', MilestoneListView.as_view(), name='project_milestones_list'),
            url(r'^add/$', MilestoneAddView.as_view(), name='project_milestones_add'),
            url(r'^(?P<milestone_pk>\d+)/$', MilestoneDetailView.as_view(), name='project_milestones_view'),
            url(r'^(?P<milestone_pk>\d+)/edit/$', MilestoneEditView.as_view(), name='project_milestones_edit'),
            url(r'^(?P<milestone_pk>\d+)/delete/$', MilestoneDeleteView.as_view(), name='project_milestones_delete'),
            url(r'^(?P<milestone_pk>\d+)/check/$', MilestoneCloseView.as_view(), name='project_milestones_check'),
            url(r'^(?P<milestone_pk>\d+)/quick_check/$', MilestoneCloseView.as_view(), { 'quick': True }, name='project_milestones_quick_check'),
            url(r'^(?P<milestone_pk>\d+)/history/$', MilestoneHistoryView.as_view(), name='project_milestones_history'),
        ])),
        url(r'^task_lists/', include([
            url(r'^list/$', TaskListListView.as_view(), name='project_task_lists_list'),
            url(r'^add/$', TaskListAddView.as_view(), name='project_task_lists_add'),
            url(r'^add/milestone-(?P<milestone_pk>\d+)/$', TaskListAddView.as_view(), name='project_task_lists_add_tomilestone'),

            url(r'^(?P<task_list_pk>\d+)/', include([
                url(r'^$', TaskListDetailView.as_view(), name='project_task_lists_view'),
                url(r'^edit/$', TaskListEditView.as_view(), name='project_task_lists_edit'),
                url(r'^delete/$', TaskListDeleteView.as_view(), name='project_task_lists_delete'),
                url(r'^copy/$', TaskListCopyView.as_view(), name='project_task_lists_copy'),
                url(r'^move/$', TaskListMoveView.as_view(), name='project_task_lists_move'),
                url(r'^history/$', TaskListHistoryView.as_view(), name='project_task_lists_history'),
                url(r'^tasks/', include([
                    url(r'^add/$', TaskAddView.as_view(), name='project_tasks_add'),
                    url(r'^(?P<task_pk>\d+)/edit/$', TaskEditView.as_view(), name='project_tasks_edit'),
                    url(r'^(?P<task_pk>\d+)/delete/$', TaskDeleteView.as_view(), name='project_tasks_delete'),
                    url(r'^(?P<task_pk>\d+)/check/$', TaskCloseView.as_view(), name='project_tasks_check'),
                    url(r'^(?P<task_pk>\d+)/quick_check/$', TaskCloseView.as_view(), { 'quick': True, 'milestone': False }, name='project_tasks_quick_check'),
                    url(r'^(?P<task_pk>\d+)/quick_check_milestone_(?P<milestone_pk>\d+)/$', TaskCloseView.as_view(), { 'quick': True }, name='project_tasks_quick_check_milestone'),
                ])),
            ])),
        ])),
        url(r'^messages/', include([
            url(r'^list/$', MessageListView.as_view(), name='project_messages_list'),
            url(r'^post/$', MessagePostView.as_view(), name='project_messages_post'),

            url(r'^(?P<message_pk>\d+)/', include([
                url(r'^$', MessageDetailView.as_view(), name='project_messages_view'),
                url(r'^edit/$', MessageEditView.as_view(), name='project_messages_edit'),
                url(r'^delete/$', MessageDeleteView.as_view(), name='project_messages_delete'),
                url(r'^quick_stick/$', MessageStickView.as_view(), name='project_messages_quick_stick'),
                url(r'^download/(?P<attachment_pk>\d+)/$', download_message_attachment, name='project_messages_download_attachment'),
                url(r'^(?P<comment_pk>\d+)/', include([
                    url(r'^edit/$', CommentEditView.as_view(), name='project_messages_comments_edit'),
                    url(r'^delete/$', CommentDeleteView.as_view(), name='project_messages_comments_delete'),
                ])),
            ])),
        ]))
    ])),
]
