from django import forms
from django_superform import SuperModelForm, InlineFormSetField
from material import Layout, Row, Column, Fieldset, Span4, Span8, Span7, Span3
from django.utils.translation import ugettext_lazy as _


class ProjectForm(forms.ModelForm):
    class Meta:
        from .models import Project
        model = Project
        fields = ('name', 'description', 'members', 'manager')

    layout = Layout(
        'name', 'description',
        Row('manager', 'members'),
    )

class ProjectEditForm(ProjectForm):
    class Meta(ProjectForm.Meta):
        pass

class ProjectAddForm(ProjectForm):
    class Meta(ProjectForm.Meta):
        pass

class TaskListForm(forms.ModelForm):
    class Meta:
        from .models import TaskList
        model = TaskList
        fields = ('name', 'description', 'due_date', 'priority', 'milestone')

    layout = Layout(
        'name', 'description',
        Row('due_date', 'priority'),
        'milestone',
    )

class TaskListCopyForm(forms.ModelForm):
    class Meta:
        from .models import TaskList
        model = TaskList
        fields = ('name', 'description', 'due_date', 'priority')

    from .models import Project
    destination_project = forms.ModelChoiceField(queryset=Project.objects.all(), label=_('Destination project'))

    keep_milestone = forms.BooleanField(label=_('Keep milestone (if possible)'), help_text=_('We will only keep milestone if you haven\'t changed project destination.'), required=False)
    keep_pinned_users = forms.BooleanField(label=_('Keep pinned members (if possible)'), help_text=_('We will only keep pinned members that are part of new project.'), required=False)
    reset_tasks_status = forms.BooleanField(label=_('Reset tasks status'), help_text=_('If checked, all done tasks will be reopened in the copy.'), required=False)
    reset_tasks_due_date = forms.BooleanField(label=_('Reset tasks due date'), required=False)
    reset_tasks_priority = forms.BooleanField(label=_('Reset tasks priority'), required=False)

    layout = Layout(
        'destination_project',
        'keep_milestone',
        'keep_pinned_users', 'reset_tasks_status', 'reset_tasks_due_date', 'reset_tasks_priority',
        Fieldset(_('New task list informations'),
            'name',
            'description',
            Row('due_date', 'priority')
        )
    )

class TaskListMoveForm(forms.ModelForm):
    class Meta:
        from .models import TaskList
        model = TaskList
        fields = ('project', 'name', 'description', 'due_date', 'priority')

    keep_pinned_users = forms.BooleanField(label=_('Keep pinned members (if possible)'), help_text=_('We will only keep pinned members that are part of new project.'), required=False)
    reset_tasks_status = forms.BooleanField(label=_('Reset tasks status'), help_text=_('If checked, all done tasks will be reopened in the copy.'), required=False)
    reset_tasks_due_date = forms.BooleanField(label=_('Reset tasks due date'), required=False)
    reset_tasks_priority = forms.BooleanField(label=_('Reset tasks priority'), required=False)

    layout = Layout(
        'project',
        'keep_pinned_users', 'reset_tasks_status', 'reset_tasks_due_date', 'reset_tasks_priority',
        Fieldset(_('New task list informations'),
            'name',
            'description',
            Row('due_date', 'priority')
        )
    )


class TaskForm(forms.ModelForm):
    class Meta:
        from .models import Task
        model = Task
        fields = ('name', 'description', 'due_date', 'priority', 'members')

    layout = Layout(
        Row(Column('name', 'description', span_columns=8), Column('members', 'due_date', 'priority', span_columns=4)),
    )

from .models import TaskList, Task
TaskFormSet = forms.inlineformset_factory(TaskList, Task, form=TaskForm, extra=10)

class TaskListAddForm(SuperModelForm):
    class Meta:
        from .models import TaskList
        model = TaskList
        fields = ('name', 'description', 'due_date', 'priority', 'milestone')
    tasks = InlineFormSetField(formset_class=TaskFormSet)

    layout = Layout(
        Fieldset(_('Task list'),
            'name', 'description',
            Row('due_date', 'priority'),
            'milestone',
        ),
        Fieldset(_('Tasks'), 'tasks'),
    )


class MilestoneForm(forms.ModelForm):
    class Meta:
        from .models import Milestone
        model = Milestone
        fields = ('name', 'description', 'due_date')




class AttachmentForm(forms.ModelForm):
    class Meta:
        from .models import Attachment
        model = Attachment
        fields = ('name', 'file')

    layout = Layout(
        Row(Span7('name'), Span3('file'), 'DELETE')
    )


from .models import Message, Attachment
AttachmentFormSet = forms.inlineformset_factory(Message, Attachment, form=AttachmentForm, can_delete=True)

class MessageForm(SuperModelForm):
    class Meta:
        from .models import Message
        model = Message
        fields = ('title', 'pinned_members', 'body', 'allow_comments')

    attachments = InlineFormSetField(formset_class=AttachmentFormSet)

    layout = Layout(
        Row(Span8('title'), Span4('allow_comments')),
        'pinned_members',
        'body',
        Fieldset(_('Attachments'), 'attachments')
    )

class CommentForm(forms.ModelForm):
    class Meta:
        from .models import Comment
        model = Comment
        fields = ('body',)
