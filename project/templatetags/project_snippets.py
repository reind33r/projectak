from django import template

register = template.Library()

@register.inclusion_tag("project/snippets/progress.html")
def progress(object):
    return { 'object': object }