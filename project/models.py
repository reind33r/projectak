from django.db import models
from django.utils.translation import ugettext_lazy as _


# DUE_DATE ORDERING WITH NULL
def order_queryset(queryset):
    if queryset.model.__name__ == 'Task':
        ordering = ['closed', 'new_milestone_due_date', 'new_task_list_due_date', '-task_list__priority', 'new_due_date', '-priority']
        milestone_due_date_field = 'task_list__milestone__due_date'
        task_list_due_date_field = 'task_list__due_date'
    elif queryset.model.__name__ == 'TaskList':
        ordering = ['new_milestone_due_date', 'new_due_date', '-priority', 'name', 'pk']
        milestone_due_date_field = 'milestone__due_date'
        task_list_due_date_field = 'due_date'
    else:
        raise ValueError('Can\'t call order_queryset() on a {} model'.format(queryset.model.__name__))


    import datetime
    from django.db.models.functions import Coalesce
    from django.db.models import Value

    the_past = datetime.datetime(9999,1,1)

    ordered_queryset = queryset.annotate(
        new_due_date=Coalesce('due_date', Value(the_past)),
        new_milestone_due_date=Coalesce(milestone_due_date_field, Value(the_past)),
        new_task_list_due_date=Coalesce(task_list_due_date_field, Value(the_past))
    ).order_by(
        *ordering
    )

    return ordered_queryset

class Project(models.Model):
    class Meta:
        verbose_name = _("Project")
        verbose_name_plural = _("Projects")
        permissions = (
            ("view_project", "Can view a project"),
            ("close_project", "Can set a project status to closed"),
            ("view_members_project", "Can view the list of members assigned to a project"),
            ("view_history_project", "Can view the history of a project"),
            ("add_milestone_project", "Can add a milestone to the project"),
            ("add_task_list_project", "Can add a task list to the project"),
            ("add_task_project", "Can add a task to the project"),
            ("stick_message_project", _("Can stick a message in Project")),
        )

    name = models.CharField(_("Name"), max_length=255)

    organization = models.ForeignKey(
        "organization.Organization",
        verbose_name=_("Organization"),
    )
    manager = models.ForeignKey(
        "organization.Contact",
        related_name="projects_as_manager",
        help_text=_("""You can add managers in "Organization" tab."""),
        null=True,
        on_delete=models.SET_NULL,
        verbose_name=_("Project manager"),
    )
    members = models.ManyToManyField(
        "organization.Contact",
        related_name="projects_as_member",
        help_text=_("""Members must first be registered in "Organization" tab"""),
        verbose_name=_("Members"),
    )

    description = models.TextField(_("Description of the project"), blank=True, null=True)

    closed = models.BooleanField(_("Closed"), default=False)
    closed_date = models.DateField(_("Closed on"), blank=True, null=True)


    original_closed = None

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        from django.core.urlresolvers import reverse
        return reverse('project_overview', kwargs={ 'project_pk': self.pk })


    def getTasks(self):
        tasks = Task.objects.filter(task_list__project=self)
        return order_queryset(tasks)

    def getCompletedTasks(self):
        tasks = Task.objects.filter(closed=True, task_list__project=self)
        return order_queryset(tasks)

    def getCompleted(self):
        tasks_count = self.getTasks().count()

        if tasks_count == 0:
            return 0

        tasks_closed_count = self.getCompletedTasks().count()

        return round(tasks_closed_count / tasks_count * 100, 2)


    def getCompletedTaskLists(self):
        task_lists = TaskList.objects.filter(closed=True, project=self)
        return order_queryset(task_lists)

    def getOpenTaskLists(self):
        task_lists = TaskList.objects.filter(closed=False, project=self)
        return order_queryset(task_lists)

    def getUpcomingMilestones(self):
        from datetime import date, timedelta
        todayPlus30 = date.today() + timedelta(days=30)

        milestones = Milestone.objects.filter(project=self, closed=False, due_date__lte=todayPlus30)

        return milestones


class TaskList(models.Model):

    class Meta:
        verbose_name = _("Task List")
        verbose_name_plural = _("Task Lists")
        ordering = ['due_date', '-priority', 'name', 'pk']
        permissions = (
            ("view_tasklist", "Can view a task list"),
            ("move_tasklist", "Can move a task list to another project"),
        )

    name = models.CharField(_("Name"), max_length=255)
    description = models.TextField(_("Description"), blank=True, null=True)

    project = models.ForeignKey("project.Project")
    milestone = models.ForeignKey("project.Milestone", blank=True, null=True, on_delete=models.SET_NULL)

    due_date = models.DateField(_("Due date"), blank=True, null=True)
    priority = models.IntegerField(_("Priority"), default=0)

    closed = models.BooleanField(_("Closed"), default=False)
    closed_date = models.DateField(_("Closed on"), blank=True, null=True)


    original_closed = None
    original_milestone = None

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        from django.core.urlresolvers import reverse
        return reverse('project_task_lists_view', kwargs={ 'project_pk': self.project.pk, 'task_list_pk': self.pk })


    def getTasks(self):
        tasks = Task.objects.filter(task_list=self)
        return order_queryset(tasks)

    def getCompletedTasks(self):
        tasks = Task.objects.filter(closed=True, task_list=self)
        return order_queryset(tasks)

    def getOpenTasks(self):
        tasks = Task.objects.filter(closed=False, task_list=self)
        return order_queryset(tasks)

    def getCompleted(self):
        tasks_count = self.getTasks().count()

        if tasks_count == 0:
            return 0

        tasks_closed_count = self.getCompletedTasks().count()

        return round(tasks_closed_count / tasks_count * 100, 2)


    def getLate(self):
        from datetime import date
        today = date.today()

        if today > self.due_date:
            return True
        else:
            return False

    def getDaysDiffFromToday(self):
        from datetime import date
        today = date.today()
        diff = self.due_date - today

        return abs(diff.days)


class Task(models.Model):

    class Meta:
        verbose_name = _("Task")
        verbose_name_plural = _("Tasks")
        ordering = ['closed', 'due_date', '-priority']
        permissions = (
            ("view_task", "Can view a task"),
            ("close_task", "Can set a task status to closed"),
            ("view_members_task", "Can view the list of members assigned to a task"),
        )

    name = models.CharField(_("Name"), max_length=255)
    description = models.TextField(_("Description of the task"), blank=True, null=True)

    task_list = models.ForeignKey("project.TaskList")
    members = models.ManyToManyField(
        "organization.Contact",
        verbose_name=_("Members"),
        blank=True,
    )

    due_date = models.DateField(_("Due date"), blank=True, null=True)
    priority = models.IntegerField(_("Priority"), default=0)

    closed = models.BooleanField(_("Closed"), default=False)
    closed_date = models.DateField(_("Closed on"), blank=True, null=True)


    original_closed = None

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        from django.core.urlresolvers import reverse
        return reverse('project_task_lists_view', kwargs={ 'project_pk': self.task_list.project.pk, 'task_list_pk': self.task_list.pk })


    def getLate(self):
        from datetime import date
        today = date.today()

        if today > self.due_date:
            return True
        else:
            return False

    def getDaysDiffFromToday(self):
        from datetime import date
        today = date.today()
        diff = self.due_date - today

        return abs(diff.days)

class Milestone(models.Model):

    class Meta:
        verbose_name = _("Milestone")
        verbose_name_plural = _("Milestones")
        ordering = ['due_date']
        permissions = (
            ("view_milestone", "Can view a milestone"),
            ("close_milestone", "Can set a milestone status to closed"),
        )

    name = models.CharField(_("Name"), max_length=255)
    due_date = models.DateField(_("Due date"))
    description = models.TextField(_("Description"), blank=True, null=True)

    project = models.ForeignKey("project.Project")

    closed = models.BooleanField(_("Closed"), default=False)
    closed_date = models.DateField(_("Closed on"), blank=True, null=True)


    original_closed = None


    def __str__(self):
        return self.name

    def get_absolute_url(self):
        from django.core.urlresolvers import reverse
        return reverse('project_milestones_view', kwargs={ 'project_pk': self.project.pk, 'milestone_pk': self.pk })


    def getLate(self):
        from datetime import date
        today = date.today()

        if today > self.due_date:
            return True
        else:
            return False

    def getDaysDiffFromToday(self):
        from datetime import date
        today = date.today()
        diff = self.due_date - today

        return abs(diff.days)

    def getCompletedTaskLists(self):
        task_lists = TaskList.objects.filter(milestone=self, closed=True)
        return task_lists

    def getOpenTaskLists(self):
        task_lists = TaskList.objects.filter(milestone=self, closed=False)
        return task_lists


    def getTasks(self):
        tasks = Task.objects.filter(task_list__milestone=self)
        return tasks

    def getCompletedTasks(self):
        tasks = Task.objects.filter(closed=True, task_list__milestone=self)
        return tasks

    def getCompleted(self):
        tasks_count = self.getTasks().count()

        if tasks_count == 0:
            return 0

        tasks_closed_count = self.getCompletedTasks().count()

        return round(tasks_closed_count / tasks_count * 100, 2)






from message.models import Message as AbstractMessage
class Message(AbstractMessage):
    project = models.ForeignKey("project.Project")

    original_sticky = None

    def get_absolute_url(self):
        from django.core.urlresolvers import reverse
        return reverse('project_messages_view', kwargs={ 'project_pk': self.project.pk, 'message_pk': self.pk })


from message.models import Comment as AbstractComment
class Comment(AbstractComment):
    message = models.ForeignKey("project.Message")

    def get_absolute_url(self):
        from django.core.urlresolvers import reverse

        return '{}?page={}#comment-{}'.format(reverse('project_messages_view', kwargs={'project_pk': self.message.project.pk, 'message_pk':self.message.pk}), self.message.get_comment_page(self), self.pk)


from message.models import Attachment as AbstractAttachment
class Attachment(AbstractAttachment):
    message = models.ForeignKey("project.Message")

    def file_path(instance, filename):
        from projectak.settings import SENDFILE_ROOT
        from django.core.files.storage import Storage
        return '{}project/{}/messages/{}/attachments/{}'.format(SENDFILE_ROOT, instance.message.project.pk, instance.message.pk, filename)

    file = models.FileField(upload_to=file_path)
