from project.logics import SingleUserPermissionLogic, ManyUsersPermissionLogic
from permission.logics import AuthorPermissionLogic

PERMISSION_LOGICS = (
    # if project manager
    ('project.Project', SingleUserPermissionLogic(
        field_name='manager__user',
        any_permission=True,
    )),
    ('project.Milestone', SingleUserPermissionLogic(
        field_name='project__manager__user',
        any_permission=True,
    )),
    ('project.TaskList', SingleUserPermissionLogic(
        field_name='project__manager__user',
        any_permission=True,
    )),
    ('project.Task', SingleUserPermissionLogic(
        field_name='task_list__project__manager__user',
        any_permission=True,
    )),

    # if organization manager
    ('project.Project', SingleUserPermissionLogic(
        field_name='organization__manager__user',
        any_permission=True,
    )),
    ('project.Milestone', SingleUserPermissionLogic(
        field_name='project__organization__manager__user',
        any_permission=True,
    )),
    ('project.Task', SingleUserPermissionLogic(
        field_name='task_list__project__organization__manager__user',
        any_permission=True,
    )),
    ('project.TaskList', SingleUserPermissionLogic(
        field_name='project__organization__manager__user',
        any_permission=True,
    )),

    # if member
    ('project.Project', ManyUsersPermissionLogic(
        field_name='members__user',
        view_permission=True,
        view_members_permission=True,
    )),
    ('project.Milestone', ManyUsersPermissionLogic(
        field_name='project__members__user',
        view_permission=True,
        close_permission=True,
    )),
    ('project.Task', ManyUsersPermissionLogic(
        field_name='task_list__project__members__user',
        view_permission=True,
        view_members_permission=True,
        close_permission=True,
    )),
    ('project.TaskList', ManyUsersPermissionLogic(
        field_name='project__members__user',
        view_permission=True,
        close_permission=True,
    )),


    ('project.Message', AuthorPermissionLogic(
        field_name='author__user',
        any_permission=True,
    )),
    ('project.Comment', AuthorPermissionLogic(
        field_name='author__user',
        any_permission=True,
    )),
)

    # def __init__(self,
    #              field_name=None,
    #              any_permission=None,
    #              change_permission=None,
    #              delete_permission=None,
    #              close_permission=None,
    #              view_members_permission=None):
