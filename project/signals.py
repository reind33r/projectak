from django.db.models.signals import post_init, post_save, post_delete, pre_delete, m2m_changed
from django.dispatch import receiver

from notification.utils import make_notification
from history.utils import add_to_history

from .models import Project, Task, TaskList, Milestone, Message, Comment

from projectak.middleware import get_current_user

def get_current_contact():
    if get_current_user() and hasattr(get_current_user(), 'contact'):
        return get_current_user().contact
    return None
def get_current_contact_pk():
    if get_current_contact():
        return get_current_contact().pk
    else:
        return -1

# CUSTOM SIGNALS
import django.dispatch
closed = django.dispatch.Signal(providing_args=['instance'])
reopened = django.dispatch.Signal(providing_args=['instance'])


#### NOTIFICATIONS ####

## Assignments / pinned
@receiver(m2m_changed, sender=Project.members.through)
def project_assignments_listener(sender, **kwargs):
    project = kwargs.get('instance')

    if kwargs.get('action') == 'post_add' and not kwargs.get('reverse'):
        from organization.models import Contact
        for pk in kwargs.get('pk_set'):
            if pk != get_current_contact_pk():
                make_notification(Contact.objects.get(pk=pk), 'project_project_assignation', project.pk)

    if kwargs.get('action') == 'post_remove' and not kwargs.get('reverse'):
        from organization.models import Contact
        for pk in kwargs.get('pk_set'):
            if pk != get_current_contact_pk():
                make_notification(Contact.objects.get(pk=pk), 'project_project_not_working_anymore', project.pk)

@receiver(m2m_changed, sender=Task.members.through)
def task_assignments_listener(sender, **kwargs):
    task = kwargs.get('instance')

    if kwargs.get('action') == 'post_add' and not kwargs.get('reverse'):
        from organization.models import Contact
        for pk in kwargs.get('pk_set'):
            if pk != get_current_contact_pk():
                make_notification(Contact.objects.get(pk=pk), 'project_task_assignation', task.pk)

    if kwargs.get('action') == 'post_remove' and not kwargs.get('reverse'):
        from organization.models import Contact
        for pk in kwargs.get('pk_set'):
            if pk != get_current_contact_pk():
                make_notification(Contact.objects.get(pk=pk), 'project_task_not_working_anymore', task.pk)


@receiver(m2m_changed, sender=Message.pinned_members.through)
def message_pinned_listener(sender, **kwargs):
    message = kwargs.get('instance')

    if kwargs.get('action') == 'post_add' and not kwargs.get('reverse'):
        from organization.models import Contact
        for pk in kwargs.get('pk_set'):
            if pk != get_current_contact_pk():
                make_notification(Contact.objects.get(pk=pk), 'project_message_tagged', message.pk)

## New objects / Deletions
@receiver(post_save, sender=TaskList)
def tl_post_save_listener(sender, **kwargs):
    tl = kwargs.get('instance')
    updated = False

    if kwargs.get('created') == True:
        add_to_history(get_current_contact(), 'project_tl_new', tl.pk)
    else:
        updated = True

        if tl.original_closed and not tl.closed:
            add_to_history(get_current_contact(), 'project_tl_reopened', tl.pk)
            updated = False
        elif tl.closed and not tl.original_closed:
            add_to_history(get_current_contact(), 'project_tl_closed', tl.pk)
            updated = False

    if tl.original_milestone != tl.milestone:
        updated = False
        if tl.original_milestone: # if original, then it's not current anymore
            add_to_history(get_current_contact(), 'project_milestone_tl_deleted', tl.original_milestone.pk, tl=tl)
            add_to_history(get_current_contact(), 'project_tl_milestone_deleted', tl.pk, milestone=tl.original_milestone)

        if tl.milestone: # it's new
            add_to_history(get_current_contact(), 'project_milestone_tl_added', tl.milestone.pk, tl=tl)
            add_to_history(get_current_contact(), 'project_tl_milestone_added', tl.pk, milestone=tl.milestone)

    if updated:
        add_to_history(get_current_contact(), 'project_tl_updated', tl.pk)


@receiver(post_save, sender=Message)
def message_added_listener(sender, **kwargs):
    message = kwargs.get('instance')

    if kwargs.get('created') == True:
        for member in message.project.members.all():
            if member.pk != get_current_contact_pk():
                make_notification(member, 'project_message_new', message.pk)

        add_to_history(get_current_contact(), 'project_message_new', message.pk)

@receiver(post_save, sender=Comment)
def comment_added_listener(sender, **kwargs):
    comment = kwargs.get('instance')

    if kwargs.get('created') == True:
        alreadyNotifiedPk = [comment.author.pk, get_current_contact_pk]

        if not comment.message.author.pk in alreadyNotifiedPk:
            make_notification(comment.message.author, 'project_message_comment', comment.message.pk)
            alreadyNotifiedPk.append(comment.message.author.pk)

        for loopComment in comment.message.comment_set.all():
            if not loopComment.author.pk in alreadyNotifiedPk:
                make_notification(loopComment.author, 'project_message_comment', comment.message.pk)
                alreadyNotifiedPk.append(loopComment.author.pk)

        add_to_history(get_current_contact(), 'project_message_comment', comment.message.pk)

@receiver(pre_delete, sender=Project)
def project_removed_listener(sender, **kwargs):
    project = kwargs.get('instance')

    for member in project.members.all():
        if member.pk != get_current_contact_pk():
            make_notification(member, 'project_project_deleted', project.pk)

    add_to_history(get_current_contact(), 'project_project_deleted', project.pk)

@receiver(pre_delete, sender=Milestone)
def milestone_removed_listener(sender, **kwargs):
    milestone = kwargs.get('instance')

    for member in milestone.project.members.all():
        if member.pk != get_current_contact_pk():
            make_notification(member, 'project_milestone_deleted', milestone.pk)

    add_to_history(get_current_contact(), 'project_milestone_deleted', milestone.pk)


@receiver(pre_delete, sender=TaskList)
def tl_removed_listener(sender, **kwargs):
    tl = kwargs.get('instance')

    add_to_history(get_current_contact(), 'project_tl_deleted', tl.pk)

@receiver(pre_delete, sender=Task)
def task_removed_listener(sender, **kwargs):
    task = kwargs.get('instance')

    for member in task.members.all():
        if member.pk != get_current_contact_pk():
            make_notification(member, 'project_task_deleted', task.pk)

    add_to_history(get_current_contact(), 'project_task_deleted', task.pk)


@receiver(pre_delete, sender=Message)
def message_removed_listener(sender, **kwargs):
    message = kwargs.get('instance')

    add_to_history(get_current_contact(), 'project_message_deleted', message.pk)


## Closed / Reopened / Pinned trick
@receiver(post_init, sender=Project)
def project_post_init_listener(sender, **kwargs):
    project = kwargs.get('instance')
    project.original_closed = project.closed

@receiver(post_save, sender=Project)
def project_post_save_listener(sender, **kwargs):
    project = kwargs.get('instance')

    if not kwargs.get('created'):
        if project.closed and not project.original_closed:
            closed.send(project.__class__, instance=project)
        elif not project.closed and project.original_closed:
            reopened.send(project.__class__, instance=project)
        else:
            add_to_history(get_current_contact(), 'project_project_updated', project.pk)
    else:
        add_to_history(get_current_contact(), 'project_project_creation', project.pk)

@receiver(post_init, sender=Task)
def task_post_init_listener(sender, **kwargs):
    task = kwargs.get('instance')
    task.original_closed = task.closed

@receiver(post_save, sender=Task)
def task_post_save_listener(sender, **kwargs):
    task = kwargs.get('instance')

    if not kwargs.get('created'):
        if task.closed and not task.original_closed: # close
            closed.send(task.__class__, instance=task)
        elif not task.closed and task.original_closed: # open
            reopened.send(task.__class__, instance=task)
        else: # other update
            for member in task.members.all():
                if member.pk != get_current_contact_pk():
                    make_notification(member, 'project_task_updated', task.pk)

            add_to_history(get_current_contact(), 'project_task_updated', task.pk)
    else:
        add_to_history(get_current_contact(), 'project_task_new', task.pk)

@receiver(post_init, sender=Milestone)
def milestone_post_init_listener(sender, **kwargs):
    milestone = kwargs.get('instance')
    milestone.original_closed = milestone.closed

@receiver(post_save, sender=Milestone)
def milestone_post_save_listener(sender, **kwargs):
    milestone = kwargs.get('instance')

    if kwargs.get('created') == True:
        for member in milestone.project.members.all():
            if member.pk != get_current_contact_pk():
                make_notification(member, 'project_milestone_new', milestone.pk)

        add_to_history(get_current_contact(), 'project_milestone_new', milestone.pk)
    else:
        if milestone.closed and not milestone.original_closed: # close
            closed.send(milestone.__class__, instance=milestone)
        elif not milestone.closed and milestone.original_closed: # open
            reopened.send(milestone.__class__, instance=milestone)
        else: # other update
            add_to_history(get_current_contact(), 'project_milestone_updated', milestone.pk)

@receiver(post_init, sender=Message)
def message_post_init_listener(sender, **kwargs):
    message = kwargs.get('instance')
    message.original_sticky = message.sticky

@receiver(post_save, sender=Message)
def message_post_save_listener(sender, **kwargs):
    message = kwargs.get('instance')

    if not message.original_sticky and message.sticky:
        add_to_history(get_current_contact(), 'project_message_sticked', message.pk)


## Closed / Reopened / Pinned (proper signals)
@receiver(closed, sender=Project)
def project_closed_listener(sender, **kwargs):
    project = kwargs.get('instance')

    for member in project.members.all():
        if member.pk != get_current_contact_pk():
            make_notification(member, 'project_project_closed', project.pk)

    add_to_history(get_current_contact(), 'project_project_closed', project.pk)

@receiver(reopened, sender=Project)
def project_reopened_listener(sender, **kwargs):
    project = kwargs.get('instance')

    for member in project.members.all():
        if member.pk != get_current_contact_pk():
            make_notification(member, 'project_project_reopened', project.pk)

    add_to_history(get_current_contact(), 'project_project_reopened', project.pk)


@receiver(closed, sender=Task)
def task_closed_listener(sender, **kwargs):
    task = kwargs.get('instance')

    for member in task.members.all():
        if member.pk != get_current_contact_pk():
            make_notification(member, 'project_task_closed', task.pk)

    add_to_history(get_current_contact(), 'project_task_closed', task.pk)

@receiver(reopened, sender=Task)
def task_reopened_listener(sender, **kwargs):
    task = kwargs.get('instance')

    for member in task.members.all():
        if member.pk != get_current_contact_pk():
            make_notification(member, 'project_task_reopened', task.pk)

    add_to_history(get_current_contact(), 'project_task_reopened', task.pk)


@receiver(closed, sender=Milestone)
def milestone_closed_listener(sender, **kwargs):
    milestone = kwargs.get('instance')

    for member in milestone.project.members.all():
        if member.pk != get_current_contact_pk():
            make_notification(member, 'project_milestone_closed', milestone.pk)

    add_to_history(get_current_contact(), 'project_milestone_closed', milestone.pk)

@receiver(reopened, sender=Milestone)
def milestone_reopened_listener(sender, **kwargs):
    milestone = kwargs.get('instance')

    for member in milestone.project.members.all():
        if member.pk != get_current_contact_pk():
            make_notification(member, 'project_milestone_reopened', milestone.pk)

    add_to_history(get_current_contact(), 'project_milestone_reopened', milestone.pk)

@receiver(post_init, sender=TaskList)
def tl_post_init_listener(sender, **kwargs):
    tl = kwargs.get('instance')
    tl.original_closed = tl.closed
    tl.original_milestone = tl.milestone



# at the end, because it's more logical in history order

@receiver(post_save, sender=Task)
@receiver(post_delete, sender=Task)
def task_reloadTaskListCompleted_listener(sender, **kwargs):
    task = kwargs.get('instance')
    task_list = task.task_list

    if task_list.getCompleted() == 100 and not task_list.closed:
        task_list.closed = True

        from datetime import date
        task_list.closed_date = date.today()
        task_list.save()


    elif task_list.getCompleted() != 100 and task_list.closed:
        task_list.closed = False
        task_list.closed_date = None
        task_list.save()
