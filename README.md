# Projectak!

## How to install

Download the archive and extract it wherever you want it to be.

Go to projectak/settings.py to configure your database, language ("en" and "fr" are supported).

Install requirements

pip install -r requirements.txt
python manage.py migrate

Configure your webserver to serve the app (see Django documentation, or nginx and uwsgi example files provided).

### Some fixes in some requirements

You have to make some changes in package files in order to make them work with Django 1.10 :

~/.virtualenvs/projectak/lib/python3.5/site-packages/django_superform/widgets.py
l63 render_to_string : dictionary -> context ; remove context_instance

~/.virtualenvs/projectak/lib/python3.5/site-packages/django_superform/fields.py
add disabled = False, initial = None


## Configure a demo account

If you want to enable a demo account, please execute python manage.py create_demo_account and go to templates/registration/login.html and uncomment the part "try Projectak with demo/demo".

## Known issues

Same <title></title> on every page

Organization logo not uploaded during registration
