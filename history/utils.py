from .models import History

def add_to_history(contact, entry_type, object_pk, tl=None, milestone=None):
    if contact:
        import datetime
        history = History(contact=contact, type=entry_type, object_pk=object_pk, date=datetime.datetime.now())

        history.original_object_name = history.get_object_display()
        history.make_bindings()

        if tl:
            history.tl = tl
        if milestone:
            history.milestone = milestone

        history.save()
