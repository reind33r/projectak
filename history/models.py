from django.db import models
from django.utils.translation import ugettext_lazy as _

class History(models.Model):
    class Meta:
        verbose_name = "History entry"
        verbose_name_plural = "History entries"
        ordering = ['-date']

    date = models.DateTimeField(_("Date"), auto_now_add=True)

    contact = models.ForeignKey("organization.Contact")

    TYPE = (
        (_('Organization'), (
            ('organization_organization_updated', _('Organization informations have been updated.')), #O
            ('organization_message_new', _('A message has been published.')), #O
            ('organization_message_deleted', _('A message has been deleted.')), #O
            ('organization_message_sticked', _('A message has been sticked.')), #O
            ('organization_message_comment', _('A comment has been posted.')), #O
            ('organization_member_new', _('Please welcome a new member!')), #O
            ('organization_member_updated', _('Member profile updated.')), #O
            ('organization_member_deleted', _('A member has left us.')), #O
        )),
        (_('Project'), (
            ('project_project_creation', _('Project creation.')), #O|P
            ('project_project_updated', _('Project informations have been updated.')), #P
            ('project_project_deleted', _('A project has been deleted.')), #O
            ('project_project_closed', _('Project has been closed.')), #O|P
            ('project_project_reopened', _('Project has been reopened.')), #O|P

            ('project_milestone_new', _('Milestone creation.')), #P|M
            ('project_milestone_updated', _('Milestone informations have been updated.')), #M
            ('project_tl_milestone_added', _('A task list has been assigned to this milestone.')), #M
            ('project_tl_milestone_deleted', _('Task list has been removed from this milestone.')), #M
            ('project_milestone_closed', _('Milestone has been completed. Congratulations!')), #P|M
            ('project_milestone_reopened', _('Milestone has been reopened.')), #P|M
            ('project_milestone_deleted', _('A milestone has been deleted.')), #P

            ('project_tl_new', _('Task list creation.')), #P|TL
            ('project_tl_updated', _('Task list informations have been updated.')), #TL
            ('project_milestone_tl_added', _('Task list has been assigned to a milestone.')), #TL
            ('project_milestone_tl_deleted', _('Task list has been removed from this milestone.')), #TL
            ('project_tl_closed', _('Task list has been completed. Congratulations!')), #P|TL
            ('project_tl_reopened', _('Task list has been reopened.')), #P|TL
            ('project_tl_deleted', _('A task list has been deleted.')), #P

            ('project_task_new', _('A task has been added.')), #TL
            ('project_task_updated', _('Task informations have been updated.')), #TL
            ('project_task_deleted', _('A task has been deleted.')), #TL
            ('project_task_closed', _('Task has been closed.')), #TL
            ('project_task_reopened', _('Task has been reopened.')), #TL

            ('project_message_new', _('A message has been posted.')), #P
            ('project_message_sticked', _('A message has been sticked.')), #P
            ('project_message_deleted', _('A message has been deleted.')), #P
            ('project_message_comment', _('A comment has been posted.')), #P
        )),
    )

    type = models.CharField(_("Type of notification"), max_length=255, choices=TYPE)

    object_pk = models.IntegerField(_("ID of concerned object"))

    original_object_name = models.CharField(_("Original object name"), max_length=255, blank=True, null=True)

    # not blank IF AND ONLY IF has to appear in this view
    project = models.ForeignKey("project.Project", blank=True, null=True, on_delete=models.SET_NULL)
    milestone = models.ForeignKey("project.Milestone", blank=True, null=True, on_delete=models.SET_NULL)
    tl = models.ForeignKey("project.TaskList", blank=True, null=True, on_delete=models.SET_NULL)
    organization = models.ForeignKey("organization.Organization", blank=True, null=True, on_delete=models.SET_NULL)

    def get_object(self):
        from django.core.exceptions import ObjectDoesNotExist
        try:
            if self.type.startswith('organization_organization'):
                from organization.models import Organization
                return Organization.objects.get(pk=self.object_pk)
            elif self.type.startswith('organization_message'):
                from organization.models import Message
                return Message.objects.get(pk=self.object_pk)
            if self.type.startswith('organization_member'):
                from organization.models import Contact
                return Contact.objects.get(pk=self.object_pk)
            elif self.type.startswith('project_project'):
                from project.models import Project
                return Project.objects.get(pk=self.object_pk)
            elif self.type.startswith('project_milestone'):
                from project.models import Milestone
                return Milestone.objects.get(pk=self.object_pk)
            elif self.type.startswith('project_tl'):
                from project.models import TaskList
                return TaskList.objects.get(pk=self.object_pk)
            elif self.type.startswith('project_task'):
                from project.models import Task
                return Task.objects.get(pk=self.object_pk)
            elif self.type.startswith('project_message'):
                from project.models import Message
                return Message.objects.get(pk=self.object_pk)
        except ObjectDoesNotExist:
            return None

    def get_object_url(self):
        if self.type.startswith('organization_organization'):
            from django.core.urlresolvers import reverse
            return reverse('myorganization')

        return self.get_object().get_absolute_url()

    def get_object_display(self):
        if self.get_object():
            if self.type.startswith('organization_organization'):
                return '{message}'.format(
                    message=self.get_object().name,
                )
            elif self.type.startswith('organization_message'):
                return '{message}'.format(
                    message=self.get_object().title,
                )
            elif self.type.startswith('organization_member'):
                return '{message}'.format(
                    message=self.get_object().user.get_full_name(),
                )
            elif self.type.startswith('project_project'):
                return '{project}'.format(
                    project=self.get_object().name,
                )
            elif self.type.startswith('project_milestone'):
                return '{milestone}'.format(
                    milestone=self.get_object().name,
                )
            elif self.type.startswith('project_tl'):
                return '{tl}'.format(
                    tl=self.get_object().name,
                )
            elif self.type.startswith('project_task'):
                return '{task}'.format(
                    task=self.get_object().name,
                )
            elif self.type.startswith('project_message'):
                return '{message}'.format(
                    message=self.get_object().title,
                )
        else:
            return self.original_object_name

    def make_bindings(self):
        # if special, then it has to be treated special!
        if self.type in ['project_milestone_tl_added', 'project_milestone_tl_deleted', 'project_tl_milestone_added', 'project_tl_milestone_deleted']:
            return


        elif self.type.startswith('organization_organization'):
            self.organization = self.get_object()
        elif self.type.startswith('organization_message'):
            self.organization = self.get_object().organization
        elif self.type.startswith('organization_member'):
            self.organization = self.get_object().organization

        elif self.type.startswith('project_project_') and not self.type.endswith('_updated'):
            self.organization = self.get_object().organization
            self.project = self.get_object()
        elif self.type.startswith('project_project_'):
            self.project = self.get_object()


        elif self.type.startswith('project_milestone_') and not (self.type.endswith('_updated') or self.type.endswith('_tl')):
            self.project = self.get_object().project
            self.milestone = self.get_object()
        elif self.type.startswith('project_milestone_'):
            self.milestone = self.get_object()


        elif self.type.startswith('project_tl_') and not self.type.endswith('_updated'):
            self.project = self.get_object().project
            self.tl = self.get_object()
        elif self.type.startswith('project_tl_'):
            self.tl = self.get_object()


        elif self.type.startswith('project_task_'):
            self.tl = self.get_object().task_list


        elif self.type.startswith('project_message_'):
            self.project = self.get_object().project
