from django.db import models
from django.utils.translation import ugettext_lazy as _

class Attachment(models.Model):

    class Meta:
        abstract = True
        verbose_name = _("Attachment")
        verbose_name_plural = _("Attachments")


    name = models.CharField(_("name"), max_length=255)
    date = models.DateTimeField(_("Date"), auto_now_add=True)

    # Implement this method to return SENDFILE_ROOT . model_unique_name . pk . filename
    # def file_path(instance, filename):
    #     from projectak.settings import SENDFILE_ROOT
    #     return '{}default/{}/{}'.format(SENDFILE_ROOT, instance.pk, filename)
    #
    # file = models.FileField(upload_to=file_path)


class Message(models.Model):

    class Meta:
        abstract = True
        verbose_name = _("Message")
        verbose_name_plural = _("Messages")
        ordering = ['-sticky', '-date']

    sticky = models.BooleanField(_("Sticky message?"), default=False)
    pinned_members = models.ManyToManyField(
        "organization.Contact",
        verbose_name=_("Pinned members"),
        related_name="%(app_label)s_messages_as_pinned",
        blank=True,
    )

    author = models.ForeignKey("organization.Contact", related_name="%(app_label)s_messages_as_author")
    title = models.CharField(_("Title"), max_length=255)
    date = models.DateTimeField(_("Date"), auto_now_add=True)
    edit_date = models.DateTimeField(_("Date"), auto_now=True)

    body = models.TextField(_("Your message"))

    # attachments

    def get_comment_page(self, comment):
        position = 0
        for comment_db in self.comment_set.all():
            position += 1
            if comment_db == comment:
                break

        import math
        return math.ceil(position / 10)

    allow_comments = models.BooleanField(_("Allow comments?"), default=True)

class Comment(models.Model):

    class Meta:
        abstract = True
        verbose_name = _("Comment")
        verbose_name_plural = _("Comments")
        ordering = ['date']

    # CHILD CLASS : message FK

    author = models.ForeignKey("organization.Contact", related_name="%(app_label)s_comments")
    body = models.TextField(_("Your message"))
    date = models.DateTimeField(_("Date"), auto_now_add=True)
    edit_date = models.DateTimeField(_("Date"), auto_now=True)

    # attachments
