from .logics import SingleUserPermissionLogic, ManyUsersPermissionLogic
from permission.logics import AuthorPermissionLogic

PERMISSION_LOGICS = (
    ('organization.Organization', SingleUserPermissionLogic(
        field_name='manager__user',
        account_type='admin',
        any_permission=True,
    )),


    ('organization.Contact', AuthorPermissionLogic(
        field_name='user',
        any_permission=True,
    )),

    ('organization.Message', AuthorPermissionLogic(
        field_name='author__user',
        any_permission=True,
    )),
    ('organization.Comment', AuthorPermissionLogic(
        field_name='author__user',
        any_permission=True,
    )),

    ('organization.Organization', ManyUsersPermissionLogic(
        field_name='members__user',
        account_type='admin',
        any_permission=True,
    )),

    ('organization.Organization', ManyUsersPermissionLogic(
        field_name='members__user',
        account_type='pm',
        add_project_permission=True,
    )),
)


# any_permission=None,
# change_permission=None,
# delete_permission=None,
# add_project_permission=None,
# manage_members=None
