from django import template

register = template.Library()

@register.inclusion_tag("organization/snippets/user_chip.html")
def user_chip(contact, display_organization=False):
    return { 'user': contact.user, 'contact': contact, 'display_organization': display_organization }

@register.inclusion_tag("organization/snippets/info.html")
def info(name, value, linebreaks=False):
    return { 'name': name, 'value': value, 'linebreaks': linebreaks }

@register.filter(name='phonenumber')
def phonenumber(value, country=None):
    if value:
        import phonenumbers
        return phonenumbers.format_number(value, phonenumbers.PhoneNumberFormat.NATIONAL)
    else:
        return None

@register.inclusion_tag("organization/snippets/info.html")
def info_phone(name, value, country=None):
    return { 'name': name, 'value': phonenumber(value), 'linebreaks': False }

@register.inclusion_tag("organization/snippets/info.html")
def info_link(name, value):
    return { 'name': name, 'value': '<a href="{value}">{value}</a>'.format(value=value), 'linebreaks': False, 'safeValue': True }


import re

def hex_to_int_color(v):
    if not v:
        return (220,220,220)

    return tuple(int(i,16) for i in re.match(
        r'^#?([0-9a-f]{2})([0-9a-f]{2})([0-9a-f]{2})$',
        v,
        flags=re.IGNORECASE).groups()
    )

def int_to_hex_color(v):
  return '#%02x%02x%02x' % v

@register.filter(name='contrasted')
def contrasted(value):
    rgb_value = hex_to_int_color(value)

    perceptiveLuminance = (0.299 * rgb_value[0] + 0.587 * rgb_value[1] + 0.114 * rgb_value[2]) / 255

    if perceptiveLuminance < 0.5:
        return '#FFFFFF'
    else:
        return '#424242'
