from django.db.models.signals import post_init, post_save, post_delete, pre_delete, m2m_changed
from django.dispatch import receiver

from notification.utils import make_notification
from history.utils import add_to_history

from .models import Organization, Message, Comment, Contact

from projectak.middleware import get_current_user

def get_current_contact():
    if get_current_user() and hasattr(get_current_user(), 'contact'):
        return get_current_user().contact
    return None
def get_current_contact_pk():
    if get_current_contact():
        return get_current_contact().pk
    else:
        return -1

# CUSTOM SIGNALS
import django.dispatch
closed = django.dispatch.Signal(providing_args=['instance'])
reopened = django.dispatch.Signal(providing_args=['instance'])


#### NOTIFICATIONS ####

## Assignments / pinned
@receiver(m2m_changed, sender=Message.pinned_members.through)
def message_pinned_listener(sender, **kwargs):
    message = kwargs.get('instance')

    if kwargs.get('action') == 'post_add' and not kwargs.get('reverse'):
        from organization.models import Contact
        for pk in kwargs.get('pk_set'):
            if pk != get_current_contact_pk():
                make_notification(Contact.objects.get(pk=pk), 'organization_message_tagged', message.pk)

# Added / Updated
@receiver(post_save, sender=Organization)
def organization_updated_listener(sender, **kwargs):
    organization = kwargs.get('instance')

    if not kwargs.get('created'):
        add_to_history(get_current_contact(), 'organization_organization_updated', organization.pk)


@receiver(post_save, sender=Contact)
def member_added_listener(sender, **kwargs):
    member = kwargs.get('instance')

    if kwargs.get('created'):
        add_to_history(get_current_contact(), 'organization_member_new', member.pk)
    else:
        add_to_history(get_current_contact(), 'organization_member_updated', member.pk)


@receiver(post_save, sender=Message)
def message_added_listener(sender, **kwargs):
    message = kwargs.get('instance')

    if kwargs.get('created') == True:
        for member in message.organization.members.all():
            if member.pk != get_current_contact_pk():
                make_notification(member, 'organization_message_new', message.pk)

        add_to_history(get_current_contact(), 'organization_message_new', message.pk)

@receiver(post_save, sender=Comment)
def comment_added_listener(sender, **kwargs):
    comment = kwargs.get('instance')

    if kwargs.get('created') == True:
        alreadyNotifiedPk = [comment.author.pk, get_current_contact_pk()]

        if not comment.message.author.pk in alreadyNotifiedPk:
            make_notification(comment.message.author, 'organization_message_comment', comment.message.pk)
            alreadyNotifiedPk.append(comment.message.author.pk)

        for loopComment in comment.message.comment_set.all():
            if not loopComment.author.pk in alreadyNotifiedPk:
                make_notification(loopComment.author, 'organization_message_comment', comment.message.pk)
                alreadyNotifiedPk.append(loopComment.author.pk)

        add_to_history(get_current_contact(), 'organization_message_comment', comment.message.pk)

@receiver(pre_delete, sender=Message)
def message_removed_listener(sender, **kwargs):
    message = kwargs.get('instance')

    add_to_history(get_current_contact(), 'organization_message_deleted', message.pk)

@receiver(pre_delete, sender=Contact)
def member_removed_listener(sender, **kwargs):
    member = kwargs.get('instance')

    add_to_history(get_current_contact(), 'organization_member_deleted', member.pk)


# Trick
@receiver(post_init, sender=Message)
def message_post_init_listener(sender, **kwargs):
    message = kwargs.get('instance')
    message.original_sticky = message.sticky

@receiver(post_save, sender=Message)
def message_post_save_listener(sender, **kwargs):
    message = kwargs.get('instance')

    if not message.original_sticky and message.sticky:
        add_to_history(get_current_contact(), 'organization_message_sticked', message.pk)
