from django.shortcuts import render, redirect, reverse, get_object_or_404

from django.views.generic.base import TemplateView
from django.views.generic.edit import UpdateView, CreateView, DeleteView
from django.contrib.auth.mixins import UserPassesTestMixin
from django.views.generic.detail import DetailView
from django.views.generic.list import ListView
from projectak.decorators.permission_required import permission_required

class MyOrganizationViewBase:
    def get_organization(self):
        return self.request.user.contact.organization

    def get_context_data(self, **kwargs):
        context = super(MyOrganizationViewBase, self).get_context_data(**kwargs)

        context['organization'] = self.get_organization()

        return context


class MyOrganizationView(MyOrganizationViewBase, TemplateView):
    template_name = "organization/myorganization/home.html"

    def get_context_data(self, **kwargs):
        context = super(MyOrganizationView, self).get_context_data(**kwargs)

        stickyQueryset = self.get_organization().message_set.filter(sticky=True)
        nonStickyQueryset = self.get_organization().message_set.filter(sticky=False)[:5]
        queryset = (stickyQueryset | nonStickyQueryset)[:max(5, stickyQueryset.count())]

        context['messages'] = queryset.all()

        return context

@permission_required('organization.change_organization')
class MyOrganizationEditView(MyOrganizationViewBase, UpdateView):
    from .models import Organization
    model = Organization
    from .forms import OrganizationForm
    form_class = OrganizationForm

    template_name = "organization/myorganization/edit.html"
    context_object_name = "organization"

    from django.core.urlresolvers import reverse_lazy
    success_url = reverse_lazy('myorganization')

    def get_object(self, queryset=None):
        return self.get_organization()


class MyOrganizationHistoryView(MyOrganizationViewBase, TemplateView):
    template_name = "organization/myorganization/history.html"

    def get_context_data(self, **kwargs):
        from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger

        context = super(MyOrganizationHistoryView, self).get_context_data(**kwargs)

        entries = self.get_organization().history_set.all()

        paginator = Paginator(entries, 25)
        page = self.request.GET.get('page')
        try:
            entries = paginator.page(page)
        except PageNotAnInteger:
            entries = paginator.page(1)
        except EmptyPage:
            entries = paginator.page(paginator.num_pages)


        context['entries'] = entries

        return context


# Members
@permission_required('organization.manage_members_organization')
class MemberAddView(MyOrganizationViewBase, CreateView):
    from .models import Contact
    model = Contact
    from .forms import ContactAddForm
    form_class = ContactAddForm

    template_name = "organization/members/add.html"

    def form_valid(self, form):
        form.set_organization(self.get_organization())
        member = form.save()
        return super(MemberAddView, self).form_valid(form)

    def get_success_url(self):
        from django.core.urlresolvers import reverse_lazy
        return reverse_lazy('organization_members_list')

#@permission_required('organization.manage_members_organization')
class MemberListView(MyOrganizationViewBase, ListView):
    from .models import Contact
    model = Contact
    context_object_name = 'contacts'

    template_name = "organization/members/list.html"

    def get_queryset(self):
        return self.get_organization().get_members()

# @permission_required('organization.manage_members_organization')
class MemberDetailView(MyOrganizationViewBase, DetailView):
    pk_url_kwarg = "member_pk"
    from .models import Contact
    model=Contact

    template_name = "organization/members/view.html"
    context_object_name = "member"

@permission_required('organization.manage_members_organization')
class MemberEditView(UserPassesTestMixin, MyOrganizationViewBase, UpdateView):
    pk_url_kwarg = "member_pk"

    from .models import Contact
    model = Contact
    from .forms import ContactForm
    form_class = ContactForm

    def form_valid(self, form):
        form.set_organization(self.get_organization())
        member = form.save()
        return super(MemberEditView, self).form_valid(form)

    template_name = "organization/members/edit.html"
    context_object_name = "member"


    raise_exception = True
    def test_func(self):
        return self.request.user.contact != self.get_object()


@permission_required('organization.manage_members_organization')
class MemberDeleteView(UserPassesTestMixin, MyOrganizationViewBase, DeleteView):
    pk_url_kwarg = "member_pk"

    from .models import Contact
    model = Contact

    template_name = "organization/members/delete.html"
    context_object_name = "member"

    def get_success_url(self):
        from django.core.urlresolvers import reverse_lazy
        return reverse_lazy('organization_members_list')

    raise_exception = True
    def test_func(self):
        return self.request.user.contact != self.get_object()



# Messages
class MessagePostView(MyOrganizationViewBase, CreateView):
    from .models import Message
    model = Message
    from .forms import MessageForm
    form_class = MessageForm

    template_name = "organization/messages/post.html"

    def form_valid(self, form):
        form.instance.organization = self.get_organization()
        form.instance.author = self.request.user.contact

        return super(MessagePostView, self).form_valid(form)

    def get_success_url(self):
        from django.core.urlresolvers import reverse_lazy
        return reverse_lazy('organization_messages_list')


    def get_context_data(self, **kwargs):
        context = super(MessagePostView, self).get_context_data(**kwargs)

        context['form'].fields['pinned_members'].queryset = self.request.user.contact.organization.get_members()

        return context

@permission_required('organization.change_message')
class MessageEditView(MyOrganizationViewBase, UpdateView):
    pk_url_kwarg = "message_pk"

    from .models import Message
    model = Message
    from .forms import MessageForm
    form_class = MessageForm

    template_name = "organization/messages/edit.html"
    context_object_name = "message"


    def get_context_data(self, **kwargs):
        context = super(MessageEditView, self).get_context_data(**kwargs)

        context['form'].fields['pinned_members'].queryset = self.request.user.contact.organization.get_members()

        return context

@permission_required('organization.delete_message')
class MessageDeleteView(MyOrganizationViewBase, DeleteView):
    pk_url_kwarg = "message_pk"

    from .models import Message
    model = Message

    template_name = "organization/messages/delete.html"
    context_object_name = "message"

    def get_success_url(self):
        from django.core.urlresolvers import reverse_lazy
        return reverse_lazy('organization_messages_list')


class MessageListView(MyOrganizationViewBase, ListView):
    from .models import Message
    model = Message
    context_object_name = 'messages'

    template_name = "organization/messages/list.html"


    def get_context_data(self, **kwargs):
        from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger

        context = super(MessageListView, self).get_context_data(**kwargs)

        messages = self.get_queryset()

        paginator = Paginator(messages, 10)
        page = self.request.GET.get('page')
        try:
            messages = paginator.page(page)
        except PageNotAnInteger:
            messages = paginator.page(1)
        except EmptyPage:
            messages = paginator.page(paginator.num_pages)

        context['messages'] = messages

        return context


    def get_queryset(self):
        return self.get_organization().message_set.all()

from .forms import CommentForm
class MessageDetailView(MyOrganizationViewBase, DetailView):
    pk_url_kwarg = "message_pk"
    from .models import Message
    model=Message

    template_name = "organization/messages/view.html"
    context_object_name = "message"


    def get_context_data(self, **kwargs):
        from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger

        context = super(MessageDetailView, self).get_context_data(**kwargs)

        comments = self.get_object().comment_set.all()

        paginator = Paginator(comments, 10)
        page = self.request.GET.get('page')
        try:
            comments = paginator.page(page)
        except PageNotAnInteger:
            comments = paginator.page(1)
        except EmptyPage:
            comments = paginator.page(paginator.num_pages)

        context['comments'] = comments
        context['form_quick_answer'] = CommentForm()

        return context

    def post(self, request, *args, **kwargs):
        if not self.get_object().allow_comments:
            from django.core.exceptions import PermissionDenied
            raise PermissionDenied

        form = CommentForm(request.POST)
        if form.is_valid():
            comment = form.save(commit=False)

            comment.message = self.get_object()

            comment.author = request.user.contact
            import datetime
            comment.date = datetime.datetime.now()

            comment.save()

            return redirect('{}?page={}#comment-{}'.format(reverse('organization_messages_view', kwargs={'message_pk':self.get_object().pk}), comment.message.get_comment_page(comment), comment.pk))

        return render(request, self.template_name, {'form_quick_answer': form})

# will return 404 if not orgnization member because of organization__pk
def download_message_attachment(request, message_pk, attachment_pk):
    from .models import Attachment
    attachment = get_object_or_404(Attachment, pk=attachment_pk, message__pk=message_pk, message__organization__pk=request.user.contact.organization.pk)

    from sendfile import sendfile
    return sendfile(request, attachment.file.path, attachment=True)


from django.views.generic import View
from django.views.generic.detail import SingleObjectMixin
@permission_required('organization.stick_message_organization')
class MessageStickView(MyOrganizationViewBase, SingleObjectMixin, View):
    pk_url_kwarg = "message_pk"
    from .models import Message
    model=Message

    def get(self, request, *args, **kwargs):
        message = self.get_object()

        message.sticky = not message.sticky

        message.save()

        message_pk = self.kwargs.get('message_pk', False)

        return redirect('organization_messages_list')


@permission_required('organization.change_comment')
class CommentEditView(MyOrganizationViewBase, UpdateView):
    pk_url_kwarg = "comment_pk"

    from .models import Comment
    model = Comment
    from .forms import CommentForm
    form_class = CommentForm

    template_name = "organization/messages/comment_edit.html"
    context_object_name = "comment"

@permission_required('organization.delete_comment')
class CommentDeleteView(MyOrganizationViewBase, DeleteView):
    pk_url_kwarg = "comment_pk"

    from .models import Comment
    model = Comment

    template_name = "organization/messages/comment_delete.html"
    context_object_name = "comment"

    def get_success_url(self):
        from django.core.urlresolvers import reverse_lazy
        return reverse_lazy('organization_messages_view', kwargs={'message_pk': self.get_object().message.pk })
