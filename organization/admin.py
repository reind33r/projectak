from django.contrib import admin

from .models import Organization, Contact

admin.site.register(Organization)
admin.site.register(Contact)