from django.db import models
from phonenumber_field.modelfields import PhoneNumberField
from colorful.fields import RGBColorField
from django.utils.translation import ugettext_lazy as _

class Organization(models.Model):

    class Meta:
        verbose_name = _("Organization")
        verbose_name_plural = _("Organizations")
        permissions = (
            ("add_project_organization", _("Can create a project")),
            ("manage_members_organization", _("Can manage the organization members")),
            ("stick_message_organization", _("Can stick a message in Organization tab")),
        )

    name = models.CharField(_("Name of the organization"), max_length=255)

    def picture_path(instance, filename):
        return 'organizations/{}/pictures/{}'.format(instance.pk, filename)
    picture = models.FileField(_("Picture"), upload_to=picture_path, default="organizations/default_picture.jpg")
    description = models.TextField(_("Description of the organization"), blank=True, null=True)

    email = models.EmailField(_("Email address"), blank=True, null=True)
    phone = PhoneNumberField(_("Phone number"), blank=True, null=True)
    fax = PhoneNumberField(_("Fax number"), blank=True, null=True)
    website = models.URLField(_("Website"), blank=True, null=True)

    address = models.TextField(_("Address"), blank=True, null=True)

    manager = models.OneToOneField("organization.Contact", related_name="my_organization", blank=True, null=True)

    def __str__(self):
        return self.name

    def get_members(self):
        return Contact.objects.filter(organization=self)

    def get_regular_members(self):
        from django.db.models import Q
        return Contact.objects.filter(Q(organization=self), Q(account_type="regular"))

    def get_project_managers(self):
        from django.db.models import Q
        return Contact.objects.filter(Q(organization=self), Q(account_type="pm"))


class Contact(models.Model):

    class Meta:
        verbose_name = _("Contact informations")
        verbose_name_plural = _("Contact informations")
        ordering = ['user__last_name', 'user__first_name']

    user = models.OneToOneField("auth.User", on_delete=models.CASCADE)

    ACCOUNT_TYPES = (
        ('regular', _('Regular account')),
        ('pm', _('Manager (can create, edit and delete all projects)')),
        ('admin', _('Organization administrator (all rights)')),
    )
    account_type = models.CharField(_("Type of account"), max_length=255, choices=ACCOUNT_TYPES, default="regular")

    phone = PhoneNumberField(_("Phone number"), blank=True, null=True)

    address = models.TextField(_("Address"), blank=True, null=True)

    organization = models.ForeignKey("organization.Organization", related_name="members")

    def avatar_path(instance, filename):
        return 'contacts/{}/avatars/{}'.format(instance.user.username, filename)

    avatar = models.FileField(_("Avatar"), upload_to=avatar_path, default="contacts/default_avatar.jpg")

    color = RGBColorField("Chip color", default="#00a2ed")

    # RELATED TO OTHER APPS
    unread_pm = models.IntegerField(_("Unread MP count"), default=0)

    def __str__(self):
        return '{name} ({type})'.format(name=self.user.get_full_name(), type=self.get_account_type_display())

    def get_projects(self, *args, **kwargs):
        from django.db.models import Q
        from project.models import Project
        projects = Project.objects.filter(organization=self.organization).filter(Q(manager=self) | Q(members=self)).distinct()

        if kwargs.get('active', None) is not None:
            projects = projects.filter(closed=not kwargs.get('active'))

        return projects

    def get_active_projects(self):
        return self.get_projects(active=True)

    def get_closed_projects(self):
        return self.get_projects(active=False)

    def get_absolute_url(self):
        from django.core.urlresolvers import reverse
        return reverse('organization_members_view', kwargs={ 'member_pk': self.pk })

    def unread_notifications(self):
        return self.notification_set.filter(contact=self, read=False).count()

from message.models import Message as AbstractMessage
class Message(AbstractMessage):
    organization = models.ForeignKey("organization.Organization")

    original_sticky = None


    def get_absolute_url(self):
        from django.core.urlresolvers import reverse
        return reverse('organization_messages_view', kwargs={ 'message_pk': self.pk })


from message.models import Comment as AbstractComment
class Comment(AbstractComment):
    message = models.ForeignKey("organization.Message")

    def get_absolute_url(self):
        from django.core.urlresolvers import reverse

        return '{}?page={}#comment-{}'.format(reverse('organization_messages_view', kwargs={'message_pk':self.message.pk}), self.message.get_comment_page(self), self.pk)


from message.models import Attachment as AbstractAttachment
class Attachment(AbstractAttachment):
    message = models.ForeignKey("organization.Message")

    def file_path(instance, filename):
        from projectak.settings import SENDFILE_ROOT
        from django.core.files.storage import Storage
        return '{}organization/{}/messages/{}/attachments/{}'.format(SENDFILE_ROOT, instance.message.organization.pk, instance.message.pk, filename)

    file = models.FileField(upload_to=file_path)
