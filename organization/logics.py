from permission.logics.base import PermissionLogic

PERMISSION_DEFAULT_FIELD_NAME = 'user'
PERMISSION_DEFAULT_ACCOUNT_TYPE = None
PERMISSION_DEFAULT_ANY_PERMISSION = False
PERMISSION_DEFAULT_CHANGE_PERMISSION = False
PERMISSION_DEFAULT_DELETE_PERMISSION = False
PERMISSION_DEFAULT_ADD_PROJECT_PERMISSION = False
PERMISSION_DEFAULT_stick_message_organization_PERMISSION = False

from abc import ABCMeta, abstractmethod
class BasePermissionLogic(PermissionLogic, metaclass=ABCMeta):
    def __init__(self,
                 field_name=None,
                 account_type=None,
                 any_permission=None,
                 change_permission=None,
                 delete_permission=None,
                 add_project_permission=None,
                 stick_message_organization=None):

        self.field_name = field_name
        self.account_type = account_type

        self.any_permission = any_permission
        self.change_permission = change_permission
        self.delete_permission = delete_permission
        self.add_project_permission = add_project_permission
        self.stick_message_organization = stick_message_organization

        if self.field_name is None:
            self.field_name = PERMISSION_DEFAULT_FIELD_NAME

        if self.account_type is None:
            self.account_type = PERMISSION_DEFAULT_ACCOUNT_TYPE

        if self.any_permission is None:
            self.any_permission = PERMISSION_DEFAULT_ANY_PERMISSION

        if self.change_permission is None:
            self.change_permission = PERMISSION_DEFAULT_CHANGE_PERMISSION

        if self.delete_permission is None:
            self.delete_permission = PERMISSION_DEFAULT_DELETE_PERMISSION

        if self.add_project_permission is None:
            self.add_project_permission = PERMISSION_DEFAULT_ADD_PROJECT_PERMISSION

        if self.stick_message_organization is None:
            self.stick_message_organization = PERMISSION_DEFAULT_stick_message_organization_PERMISSION

    @abstractmethod
    def _has_perm_user_field_check(self, user_obj, obj):
        pass

    def has_perm(self, user_obj, perm, obj=None):
        if not user_obj.is_authenticated():
            return False

        change_permission = self.get_full_permission_string('change')
        delete_permission = self.get_full_permission_string('delete')

        add_project_permission = self.get_full_permission_string('add_project')

        if obj is None or self._has_perm_user_field_check(user_obj, obj, self.account_type):
            if self.any_permission:
                return True

            elif perm == change_permission:
                return self.change_permission

            elif perm == delete_permission:
                return self.delete_permission

            elif perm == add_project_permission:
                return self.add_project_permission

            elif perm == stick_message_organization:
                return self.stick_message_organization

        return False


class SingleUserPermissionLogic(BasePermissionLogic):
    def _has_perm_user_field_check(self, user_obj, obj, account_type):
        from permission.utils.field_lookup import field_lookup
        user_from_obj = field_lookup(obj, self.field_name)
        if user_from_obj == user_obj and user_obj.contact.account_type == account_type:
            return True

        return False


class ManyUsersPermissionLogic(BasePermissionLogic):
    def _has_perm_user_field_check(self, user_obj, obj, account_type):
        from permission.utils.field_lookup import field_lookup
        users_from_obj = field_lookup(obj, self.field_name)

        if hasattr(users_from_obj, 'all'):
            users_from_obj = users_from_obj.all()

        if user_obj in users_from_obj and user_obj.contact.account_type == account_type:
            return True

        return False
