from django.conf.urls import url, include

from . import views
from .views import (MyOrganizationView, MyOrganizationEditView, MyOrganizationHistoryView,
                    MemberListView, MemberDetailView, MemberAddView, MemberEditView, MemberDeleteView,
                    MessageListView, MessagePostView, MessageDetailView, MessageEditView, MessageDeleteView, MessageStickView, CommentEditView, CommentDeleteView, download_message_attachment)

urlpatterns = [
    url(r'^$', MyOrganizationView.as_view(), name='myorganization'),
    url(r'^edit/$', MyOrganizationEditView.as_view(), name='myorganization_edit'),
    url(r'^history/$', MyOrganizationHistoryView.as_view(), name='myorganization_history'),
    url(r'^members/', include([
        url(r'^list/$', MemberListView.as_view(), name='organization_members_list'),
        url(r'^add/$', MemberAddView.as_view(), name='organization_members_add'),

        url(r'^(?P<member_pk>\d+)/', include([
            url(r'^$', MemberDetailView.as_view(), name='organization_members_view'),
            url(r'^edit/$', MemberEditView.as_view(), name='organization_members_edit'),
            url(r'^delete/$', MemberDeleteView.as_view(), name='organization_members_delete'),
        ])),
    ])),
    url(r'^messages/', include([
        url(r'^list/$', MessageListView.as_view(), name='organization_messages_list'),
        url(r'^post/$', MessagePostView.as_view(), name='organization_messages_post'),

        url(r'^(?P<message_pk>\d+)/', include([
            url(r'^$', MessageDetailView.as_view(), name='organization_messages_view'),
            url(r'^edit/$', MessageEditView.as_view(), name='organization_messages_edit'),
            url(r'^delete/$', MessageDeleteView.as_view(), name='organization_messages_delete'),
            url(r'^quick_stick/$', MessageStickView.as_view(), name='organization_messages_quick_stick'),
            url(r'^download/(?P<attachment_pk>\d+)/$', download_message_attachment, name='organization_messages_download_attachment'),
            url(r'^(?P<comment_pk>\d+)/', include([
                url(r'^edit/$', CommentEditView.as_view(), name='organization_messages_comments_edit'),
                url(r'^delete/$', CommentDeleteView.as_view(), name='organization_messages_comments_delete'),
            ])),
        ])),
    ]))
]
