from django import forms
from material import Layout, Row, Column, Fieldset, Span4, Span8, Span7, Span3, Span
from django.utils.translation import ugettext_lazy as _

from django_superform import SuperModelForm, ModelFormField, InlineFormSetField

class OrganizationForm(forms.ModelForm):
    class Meta:
        from .models import Organization
        model = Organization
        fields = ('name', 'picture', 'description', 'address', 'email', 'website', 'phone', 'fax')


    layout = Layout(
        Row('name', 'picture'),
        'description',
        Fieldset(
            _('Contact information'),
            'address',
            Row('email', 'website'),
            Row('phone', 'fax'),
        ),
    )

from django.core import validators
class ContactForm(forms.ModelForm):
    class Meta:
        from .models import Contact
        model = Contact
        fields = ('account_type', 'phone', 'address', 'avatar', 'color')

    _organization = None

    from .models import Contact
    account_type = forms.ChoiceField(label=_("Account type"), choices=Contact.ACCOUNT_TYPES)

    user_first_name = forms.CharField(
        label=_("first name"),
        max_length=30,
    )
    user_last_name = forms.CharField(
        label=_("last name"),
        max_length=30,
    )

    user_email = forms.EmailField(label=_("Email"), required=False)

    user_password1 = forms.CharField(label=_("Password"), widget = forms.PasswordInput, required=False, help_text = _("Leave blank if unchanged."))

    user_password2 = forms.CharField(label=_("Password confirmation"), widget = forms.PasswordInput, required=False, help_text = _("Enter the same password as above, for verification."))


    def __init__(self, *args, **kwargs):
        super(ContactForm, self).__init__(*args, **kwargs)

        from django.contrib.auth.models import User
        try:
            user = self.instance.user
            self.initial['user_first_name'] = user.first_name
            self.initial['user_last_name'] = user.last_name
            self.initial['user_email'] = user.email
        except User.DoesNotExist:
            pass

    def set_organization(self, organization):
        self._organization = organization


    def clean_user_password2(self):
        password1 = self.cleaned_data.get("user_password1")
        password2 = self.cleaned_data.get("user_password2")
        if password1 and password2 and password1 != password2:
            raise forms.ValidationError(
                self.error_messages['password_mismatch'],
                code='password_mismatch',
            )
        return password2

    def save(self, commit=True):
        contact = super(ContactForm, self).save(commit=False)

        from django.contrib.auth.models import User
        try:
            contact.user = contact.user
        except User.DoesNotExist:
            contact.user = User()

        contact.user.email = self.cleaned_data['user_email']
        contact.user.first_name = self.cleaned_data['user_first_name']
        contact.user.last_name = self.cleaned_data['user_last_name']

        if self.cleaned_data["user_password1"]:
            contact.user.set_password(self.cleaned_data["user_password1"])

        if self._organization is None and commit:
            raise Exception('Si commit, vous devez donner la compagnie de l\'employé avec Form.set_organization()')

        if commit:
            contact.organization = self._organization
            # trick to reload pk...
            user = contact.user
            user.save()
            contact.user = user
            contact.save()

        return contact

    layout = Layout(
        'account_type',
        Row('avatar', 'color'),
        Fieldset(
            _('Contact informations'),
            Row('user_first_name', 'user_last_name'),
            'address',
            Row('user_email', 'phone'),
        ),
        Fieldset(_('Authentication informations'),
            Row('user_password1', 'user_password2'),
        ),
    )

class ContactAddForm(ContactForm):
    class Meta(ContactForm.Meta):
        pass

    error_messages = {
        'password_mismatch': _("The two password fields didn't match."),
    }

    user_username = forms.CharField(
        label=_("username"),
        max_length=30,
        help_text=_('Required. 30 characters or fewer. Letters, digits and @/./+/-/_ only.'),
        validators=[
            validators.RegexValidator(r'^[\w.@+-]+$',
                                      _('Enter a valid username. '
                                        'This value may contain only letters, numbers '
                                        'and @/./+/-/_ characters.'), 'invalid'),
        ],
    )

    user_password1 = forms.CharField(label=_("Password"), widget = forms.PasswordInput)

    user_password2 = forms.CharField(label=_("Password confirmation"), widget = forms.PasswordInput, help_text = _("Enter the same password as above, for verification."))

    def clean_user_username(self):
        if not self.instance or not self.instance.pk:
            from django.contrib.auth.models import User
            if User.objects.filter(username=self.cleaned_data.get('user_username')):
                raise forms.ValidationError(_('A user with that username already exists.'))

        return self.cleaned_data.get('user_username')

    def clean_user_password2(self):
        password1 = self.cleaned_data.get("user_password1")
        password2 = self.cleaned_data.get("user_password2")
        if password1 and password2 and password1 != password2:
            raise forms.ValidationError(
                self.error_messages['password_mismatch'],
                code='password_mismatch',
            )
        return password2

    def save(self, commit=True):
        contact = super(ContactAddForm, self).save(commit=False)

        contact.user.username = self.cleaned_data['user_username']
        contact.user.set_password(self.cleaned_data["user_password1"])

        return super(ContactAddForm, self).save(commit=commit)


    layout = Layout(
        'account_type',
        Row('avatar', 'color'),
        Fieldset(_('Authentication informations'),
            Row('user_username'),
            Row('user_password1', 'user_password2'),
        ),
        Fieldset(_('Contact informations'),
            Row('user_first_name', 'user_last_name'),
            'address',
            Row('user_email', 'phone'),
        ),
    )



class AttachmentForm(forms.ModelForm):
    class Meta:
        from .models import Attachment
        model = Attachment
        fields = ('name', 'file')

    layout = Layout(
        Row(Span7('name'), Span3('file'), 'DELETE')
    )


from .models import Message, Attachment
AttachmentFormSet = forms.inlineformset_factory(Message, Attachment, form=AttachmentForm, can_delete=True)

class MessageForm(SuperModelForm):
    class Meta:
        from .models import Message
        model = Message
        fields = ('title', 'pinned_members', 'body', 'allow_comments')

    attachments = InlineFormSetField(formset_class=AttachmentFormSet)

    layout = Layout(
        Row(Span8('title'), Span4('allow_comments')),
        'pinned_members',
        'body',
        Fieldset(_('Attachments'), 'attachments')
    )

class CommentForm(forms.ModelForm):
    class Meta:
        from .models import Comment
        model = Comment
        fields = ('body',)
