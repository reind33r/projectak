from django.apps import AppConfig


class PmConfig(AppConfig):
    name = 'pm'

    def ready(self):
        import pm.signals