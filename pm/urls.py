from django.conf.urls import url, include

from .views import (PMListView,
                    StartConversationView, ConversationView)

urlpatterns = [
    url(r'^$', PMListView.as_view(), name='pm_list'),
    url(r'^add/$', StartConversationView.as_view(), name='pm_start_conversation'),
    url(r'^(?P<conversation_pk>\d+)/', include([
        url(r'^$', ConversationView.as_view(), name='pm_conversation'),
        url(r'^answer/$', PMListView.as_view(), name='pm_conversation_answer'),
        url(r'^toggle-hidden/$', PMListView.as_view(), name='pm_conversation_toggle_hidden'),
    ])),
]