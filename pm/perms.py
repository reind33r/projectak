from .logics import ManyUsersPermissionLogic

PERMISSION_LOGICS = (
    ('pm.Conversation', ManyUsersPermissionLogic(
        field_name='contacts',
        any_permission=True,
    )),
)