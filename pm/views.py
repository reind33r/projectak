from django.shortcuts import render, get_object_or_404, redirect, reverse
import datetime

from django.views.generic.list import ListView
from django.views.generic.detail import DetailView
from django.views.generic.base import TemplateView

from .forms import PMAnswerForm, ConversationForm

from projectak.decorators.permission_required import permission_required

class ConversationViewBase:
    def get_conversation(self):
        conversation_pk = self.kwargs.get('conversation_pk', False)
        from .models import Conversation
        return get_object_or_404(Conversation, pk=conversation_pk)

    def get_context_data(self, **kwargs):
        context = super(ConversationViewBase, self).get_context_data(**kwargs)

        context['conversation'] = self.get_conversation()

        return context


class PMListView(ListView):
    from .models import Conversation
    model = Conversation
    context_object_name = 'conversations'

    template_name = "pm/list.html"

    def get_queryset(self):
        return self.request.user.contact.conversation_set.all()

@permission_required('pm.view_and_answer')
class ConversationView(ConversationViewBase, DetailView):
    pk_url_kwarg = "conversation_pk"
    from .models import Conversation
    model = Conversation

    template_name = "pm/conversation.html"
    context_object_name = "conversation"

    def get_context_data(self, **kwargs):
        from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger

        context = super(ConversationView, self).get_context_data(**kwargs)

        pms = self.get_object().pm_set.all()

        paginator = Paginator(pms, 10)
        page = self.request.GET.get('page')
        try:
            pms = paginator.page(page)
        except PageNotAnInteger:
            pms = paginator.page(1)
        except EmptyPage:
            pms = paginator.page(paginator.num_pages)

        contact = self.request.user.contact

        context['unread_pms'] = []

        for pm in pms:
            if contact not in pm.read_by.all():
                pm.read_by.add(contact)
                contact.unread_pm += -1
                context['unread_pms'].append(pm)

        contact.save()

        context['pms'] = pms
        context['form_quick_answer'] = PMAnswerForm()

        return context

    def post(self, request, *args, **kwargs):
        form = PMAnswerForm(request.POST)
        if form.is_valid():
            # check if no new messages
            pm = form.save(commit=False)

            pm.conversation = self.get_object()

            pm.sender = request.user.contact
            pm.date = datetime.datetime.now()

            pm.save()

            return redirect('{}?page={}#pm-{}'.format(reverse('pm_conversation', kwargs={'conversation_pk':self.get_object().pk}), pm.conversation.get_pm_page(pm), pm.pk))

        return render(request, self.template_name, {'form_quick_answer': form})


class StartConversationView(TemplateView):
    from .models import Conversation
    model = Conversation

    template_name = "pm/start_conversation.html"

    def get_context_data(self, **kwargs):
        context = super(StartConversationView, self).get_context_data(**kwargs)

        initial = {}

        if self.request.GET.get('to', False):
            from organization.models import Contact
            try:
                recipient = Contact.objects.get(pk=self.request.GET.get('to'))
                initial = { 'contacts': { recipient } }
            except Contact.DoesNotExist:
                pass

        context['form_conversation'] = ConversationForm(prefix="conversation", initial=initial)
        context['form_pm'] = PMAnswerForm(prefix="pm")

        from organization.models import Contact
        context['form_conversation'].fields['contacts'].queryset = Contact.objects.filter(organization=self.request.user.contact.organization).exclude(pk=self.request.user.contact.pk)

        return context

    def post(self, request, *args, **kwargs):
        form_conversation = ConversationForm(request.POST, prefix="conversation")
        form_pm = PMAnswerForm(request.POST, prefix="pm")

        if form_conversation.is_valid() and form_pm.is_valid():
            conversation = form_conversation.save()

            conversation.contacts.add(request.user.contact)

            pm = form_pm.save(commit=False)

            pm.conversation = conversation

            pm.sender = request.user.contact
            pm.date = datetime.datetime.now()

            pm.save()

            return redirect('pm_conversation', conversation.pk)

        return render(request, self.template_name, {'form_conversation': form_conversation, 'form_pm': form_pm})
