from django.db import models
from django.utils.translation import ugettext_lazy as _


class Conversation(models.Model):
    class Meta:
        verbose_name = "Conversation"
        verbose_name_plural = "Conversations"
        ordering = ['-last_answer_date']
        permissions = (
            ("view_and_answer", "Can view and answer a conversation"),
        )

    subject = models.CharField(_("Subject"), max_length=255)
    quick_description = models.TextField(_("Quick description"), blank=True, null=True)

    contacts = models.ManyToManyField("organization.Contact", verbose_name=_("Participants"))

    last_answer_date = models.DateTimeField(_("Date of last answer"), auto_now_add=True, blank=True)

    def get_last_read_pm(self, contact):
        for pm in self.pm_set.all().order_by('-date'):
            if contact in pm.read_by.all():
                return pm

        return self.pm_set.first()

    def get_pm_page(self, pm):
        position = 0
        for pm_db in self.pm_set.all():
            position += 1
            if pm_db == pm:
                break

        import math
        return math.ceil(position / 10)

class PM(models.Model):

    class Meta:
        verbose_name = "PM"
        verbose_name_plural = "PMs"
        ordering = ['date']
        get_latest_by = "date"

    conversation = models.ForeignKey("pm.Conversation")

    sender = models.ForeignKey("organization.Contact")
    date = models.DateTimeField(_("Sent on"))

    body = models.TextField(_("Your message"))

    read_by = models.ManyToManyField("organization.Contact", verbose_name=_("Read by"), related_name="read_pm")
