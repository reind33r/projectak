from django import forms
from .models import Conversation, PM

from material import Layout, Row, Column, Fieldset, Span4, Span8

class PMAnswerForm(forms.ModelForm):
    class Meta:
        model = PM
        fields = ('body',)

class ConversationForm(forms.ModelForm):
    class Meta:
        from .models import Conversation
        model = Conversation
        fields = ('subject', 'quick_description', 'contacts')

    layout = Layout(
        Row(Span8('subject'), Span4('contacts')),
        'quick_description',
    )
