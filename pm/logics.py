from permission.logics.base import PermissionLogic

PERMISSION_DEFAULT_FIELD_NAME = 'user'
PERMISSION_DEFAULT_ANY_PERMISSION = False

from abc import ABCMeta, abstractmethod
class BasePermissionLogic(PermissionLogic, metaclass=ABCMeta):
    def __init__(self,
                 field_name=None,
                 any_permission=None,):

        self.field_name = field_name

        self.any_permission = any_permission

        if self.field_name is None:
            self.field_name = PERMISSION_DEFAULT_FIELD_NAME

        if self.any_permission is None:
            self.any_permission = PERMISSION_DEFAULT_ANY_PERMISSION

    @abstractmethod
    def _has_perm_user_field_check(self, user_obj, obj):
        pass

    def has_perm(self, user_obj, perm, obj=None):
        if not user_obj.is_authenticated():
            return False

        if obj is None or self._has_perm_user_field_check(user_obj, obj):
            if self.any_permission:
                return True

        return False

class ManyUsersPermissionLogic(BasePermissionLogic):
    def _has_perm_user_field_check(self, user_obj, obj):
        from permission.utils.field_lookup import field_lookup
        users_from_obj = field_lookup(obj, self.field_name)

        if hasattr(users_from_obj, 'all'):
            users_from_obj = users_from_obj.all()

        if user_obj.contact in users_from_obj:
            return True

        return False