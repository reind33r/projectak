from django.db.models.signals import post_save
from django.dispatch import receiver

from .models import PM, Conversation

@receiver(signal=post_save, sender=PM)
def on_pm_post_save(sender, **kwargs):
    pm = kwargs['instance']

    conversation = pm.conversation
    conversation.last_answer_date = pm.date
    conversation.save()

    pm.read_by.add(pm.sender)

    for contact in conversation.contacts.all():
        if contact != pm.sender:
            contact.unread_pm += 1
            contact.save()