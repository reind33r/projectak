from django import template

register = template.Library()

@register.filter
def has_read(contact, conversation):
    for pm in conversation.pm_set.all():
        if contact not in pm.read_by.all():
            return False

    return True

@register.filter
def last_read_pm(contact, conversation):
    return conversation.get_last_read_pm(contact)

@register.filter
def pm_page(pm):
    return pm.conversation.get_pm_page(pm)