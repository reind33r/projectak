from .models import Notification

def make_notification(contact, notification_type, object_pk):
    import datetime
    notification = Notification(contact=contact, type=notification_type, object_pk=object_pk, date=datetime.datetime.now())

    notification.original_object_name = notification.get_object_display()

    if notification.get_project():
        notification.original_project_name = notification.get_project().name

    notification.save()
