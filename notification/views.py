from django.shortcuts import render, get_object_or_404, redirect, reverse
import datetime

from django.views.generic.list import ListView

class NotificationsListView(ListView):
    from .models import Notification
    model = Notification
    context_object_name = 'notifications'

    template_name = "notification/list.html"

    def get_queryset(self):
        return self.request.user.contact.notification_set.all()


    def get_context_data(self, **kwargs):
        from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger

        context = super(NotificationsListView, self).get_context_data(**kwargs)

        notifications = self.get_queryset()

        paginator = Paginator(notifications, 10)
        page = self.request.GET.get('page')
        try:
            notifications = paginator.page(page)
        except PageNotAnInteger:
            notifications = paginator.page(1)
        except EmptyPage:
            notifications = paginator.page(paginator.num_pages)

        context['unread_notifications'] = []
        for notification in self.get_queryset().filter(read=False):
            notification.read = True
            context['unread_notifications'].append(notification)
            notification.save()

        context['notifications'] = notifications

        return context
