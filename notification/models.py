from django.db import models
from django.utils.translation import ugettext_lazy as _

class Notification(models.Model):
    class Meta:
        verbose_name = "Notification"
        verbose_name_plural = "Notifications"
        ordering = ['-date']

    date = models.DateTimeField(_("Date"), auto_now_add=True)

    contact = models.ForeignKey("organization.Contact")
    read = models.BooleanField(_("Read?"), default=False)

    TYPE = (
        (_('Organization'), (
            ('organization_message_new', _('A message has been published in your organization tab.')),
            ('organization_message_tagged', _('You have been tagged in an organization message.')),
            ('organization_message_comment', _('A comment has been posted in an organization message you follow.')),
        )),
        (_('Project'), (
            ('project_project_assignation', _('You have been assigned to a new project.')),
            ('project_project_not_working_anymore', _('You are not working anymore on a project.')),
            ('project_project_closed', _('A project you were assigned to has been completed: congratulations!')),
            ('project_project_reopened', _('A closed project you are assigned to has been reopened.')),
            ('project_project_deleted', _('A project you were assigned to has been removed.')),
            ('project_milestone_new', _('A milestone has been added to a project you are working on.')),
            ('project_milestone_deleted', _('A milestone has been removed from a project you are working on.')),
            ('project_milestone_closed', _('A milestone has been closed in a project you are working on.')),
            ('project_milestone_reopened', _('A milestone has been reopened in a project you are working on.')),
            ('project_task_assignation', _('You have been assigned to a task.')),
            ('project_task_updated', _('A task you are assigned to has been updated.')),
            ('project_task_not_working_anymore', _('You are not working anymore on a task.')),
            ('project_task_deleted', _('A task you were assigned to has been deleted.')),
            ('project_task_closed', _('A task you were assigned to has been closed.')),
            ('project_task_reopened', _('A closed task you are assigned to has been reopened.')),
            ('project_message_new', _('A message has been posted on a project you are working on.')),
            ('project_message_tagged', _('You have been tagged in a project message.')),
            ('project_message_comment', _('A comment has been posted in a project message you follow.')),
        )),
    )

    type = models.CharField(_("Type of notification"), max_length=255, choices=TYPE)

    object_pk = models.IntegerField(_("ID of concerned object"))

    original_object_name = models.CharField(_("Original object name"), max_length=255, blank=True, null=True)
    original_project_name = models.CharField(_("Original project name"), max_length=255, blank=True, null=True)

    def get_object(self):
        from django.core.exceptions import ObjectDoesNotExist
        try:
            if self.type.startswith('organization_message'):
                from organization.models import Message
                return Message.objects.get(pk=self.object_pk)
            elif self.type.startswith('project_project'):
                from project.models import Project
                return Project.objects.get(pk=self.object_pk)
            elif self.type.startswith('project_milestone'):
                from project.models import Milestone
                return Milestone.objects.get(pk=self.object_pk)
            elif self.type.startswith('project_task'):
                from project.models import Task
                return Task.objects.get(pk=self.object_pk)
            elif self.type.startswith('project_message'):
                from project.models import Message
                return Message.objects.get(pk=self.object_pk)
        except ObjectDoesNotExist:
            return None

    def get_project(self):
        if self.get_object():
            if self.type.startswith('organization_message'):
                return None
            elif self.type.startswith('project_project'):
                return self.get_object()
            elif self.type.startswith('project_milestone'):
                return self.get_object().project
            elif self.type.startswith('project_task'):
                return self.get_object().task_list.project
            elif self.type.startswith('project_message'):
                return self.get_object().project
            else:
                return None
        else:
            return None

    def i_am_a_test(self):
        if self.get_project():
            return self.get_project().name
        elif self.original_project_name:
            return self.original_project_name
        else:
            return None

    def get_object_url(self):
        return self.get_object().get_absolute_url()

    def get_object_display(self):
        if self.type.startswith('organization_message'):
            return '{message}'.format(
                message=self.get_object().title,
            )
        elif self.type.startswith('project_project'):
            return '{project}'.format(
                project=self.get_object().name,
            )
        elif self.type.startswith('project_milestone'):
            return '{milestone}'.format(
                milestone=self.get_object().name,
            )
        elif self.type.startswith('project_task'):
            return '"{task}" in "{task_list}"'.format(
                task=self.get_object().name,
                task_list=self.get_object().task_list.name
            )
        elif self.type.startswith('project_message'):
            return '{message}'.format(
                message=self.get_object().title,
            )
