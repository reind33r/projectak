from django.conf.urls import url, include

from .views import (NotificationsListView)

urlpatterns = [
    url(r'^$', NotificationsListView.as_view(), name='notifications_list'),
]
