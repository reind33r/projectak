��         	    �      �  R   �  R   <  U   �  W   �  d   =  B   �  B   �  8   (  8   a  :   �  :   �  =     ;   N  �   �  2   ,  2   _  6   �  3   �  7   �  7   5  N   m  R   �  ;        K  \   _  v   �  z   3  ?   �  I   �  7   8   r   p   b   �      F!     V!     i!     w!  7   �!  4   �!  :   �!  @   9"     z"     �"     �"  :   �"     #  6   #     T#     r#  ;   �#  <   �#     $  ?   %$  >   e$     �$  C   �$  0   %     5%     L%  0   e%     �%  ,   �%  ,   �%  -   &  )   <&     f&     l&     y&     �&     �&     �&     �&  
   �&     �&     �&     �&     �&  
   �&     �&     
'     '     ;'     B'     I'     L'  #   a'  '   �'     �'     �'  	   �'     �'     �'  -   �'  	   (     &(     9(     M(     b(     g(     v(     {(  	   �(     �(     �(     �(     �(     �(     �(     )     ")     :)     N)     V)     _)     d)     y)     �)     �)     �)     �)     �)  ^   �)  3   +*  
   _*  %   j*  E   �*  	   �*     �*     �*     �*  8   +     =+     Q+  !   n+     �+     �+     �+     �+     �+  2   �+     ,     ,     1,  6   9,     p,     x,  	   �,     �,  .   �,     �,  )   �,  
   -      -     %-     4-     ;-     K-     P-     i-     �-     �-     �-     �-     �-     �-  !   �-     .  	   $.     ..     F.     M.     V.     d.     i.  '   v.  ,   �.     �.     �.     �.     �.     /     '/     0/     4/     A/     O/     X/     n/     {/     �/  S   �/     �/     0  	   0     0     %0     60     >0     P0     b0     {0  '   �0     �0     �0     �0  @   �0  8   (1     a1     j1     |1     �1     �1     �1     �1     �1     �1     �1  E   �1     "2     72     L2  	   _2     i2     q2     �2     �2     �2     �2  	   �2  
   �2     �2     �2  $   �2  	   3     3  +   03  .   \3  /   �3     �3  )   �3  
   4     4  M   4  e   a4  �   �4  %   S5     y5     5     �5     �5     �5  �   �5     _6     l6  G   x6  >   �6     �6     7     7  �   (7  )   �7  &   �7  !   &8  +   H8  �   t8  >   79  ?   v9  T   �9  (   :  !   4:  E   V:  *   �:  0   �:     �:  -   ;     ;;     H;     g;     �;     �;  
   �;  	   �;     �;     �;     �;  �  �;     G=     g=     �=  6   �=  ,   �=     	>     )>     H>     f>     �>     �>     �>     �>  �   �>     �?     �?     �?     �?  ,   �?  ,   )@  @   V@  @   �@  ,   �@     A  b   A  �   tA  �   �A  ?   B  N   �B  <   C  �   KC  t   �C     BD     XD     qD     �D  ?   �D  G   �D  H   "E  M   kE     �E     �E     �E  D   F     VF  <   qF     �F      �F  A   �F  F   -G     tG  @   �G  C   �G     H  D   2H  ;   wH     �H     �H  3   �H  (   #I  C   LI  @   �I  C   �I  2   J     HJ     UJ     dJ     sJ     {J     �J     �J     �J     �J     �J     �J  	   K     K     K     -K     GK     gK     nK     uK     yK  )   �K  <   �K  +   �K     "L     +L     7L     CL  8   PL     �L     �L     �L     �L     �L     �L     �L     �L     M  	   (M     2M     LM     fM     �M     �M     �M     �M     �M     �M     N     N     N     /N     IN     bN     vN     �N     �N  h   �N  M   O     TO  -   cO  y   �O     P  
   P     "P     *P  I   BP     �P      �P  0   �P     �P     Q  	   'Q     1Q     >Q  >   UQ     �Q  (   �Q     �Q  K   �Q     (R     0R     9R     ?R  !   UR     wR  2   �R     �R  	   �R     �R     �R     S     S     S  -   3S     aS     qS     �S     �S     �S     �S     �S     �S     
T     T     /T     8T     ?T     MT     TT      aT  ;   �T     �T     �T     �T     �T     U     "U     1U     BU     OU     ^U     kU     �U     �U     �U  |   �U  &   2V     YV  
   kV  	   vV     �V     �V     �V     �V     �V     �V  3   �V     'W     =W     OW  J   XW  D   �W     �W     �W     X     
X     X     "X     4X     PX     \X     lX  K   tX  +   �X  )   �X  $   Y     ;Y  
   IY     TY     oY     �Y     �Y     �Y     �Y     �Y     �Y     �Y  6   �Y     /Z  !   @Z  2   bZ  0   �Z  1   �Z  %   �Z  @   [     _[     q[  p   y[  �   �[  �   s\  ,   1]     ^]     d]     s]     �]     �]  �   �]     Z^     i^  V   x^  Z   �^     *_     8_  "   D_  �   g_  1   F`  2   x`  +   �`  B   �`  �   a  N   b  O   ab  X   �b  1   
c  *   <c  J   gc  4   �c  9   �c     !d  8   =d     vd  &   �d     �d  	   �d     �d     �d     �d     �d     �d     �d     �   �   �   �   �   �   6   �   \      �       '   �           �       �       �       �   ?   �       8   T       �             �   :       F   �         l   t        &   	   �       �                �   �   �                 �       �      d   n   �   �   V   #       �      I          �   a      y      Z   �   w     �           �     �   s           ]        �     S   �   �   �       M   2   �       �   (       �   �      �           !   A   �   G         �      �   �          �   �            <   .   �   H   
  x           {   �       �                7   �       "   �      �       �   ~       o   1   �       )            �   �   r   �   D   W   �   `   �   j   �   9   �   *   �   �   �       �      �   i       _   %       4   �         c   e   �       N   >             =   �   �   K             @   �   �      �   �              �   	  z           �   �   u           �       
   �       �            |                                 �   �   g   �              P       Q   �   �      �   �       ,       $   }       b                  /   �          B       �   �   +   �   �     �   �   �   �       �   f       U   -   �   �       [      �       �     h   �       �   �   k                �       �       �   q   �   �      �   �       3   ^   �       �   E       �   �      O      �           R           Y   ;     �   �       �   �   C   �   5   m   p      �   L       0               �   X   J       �   v   �   �     �    
                            (%(daysLeft)s days late)
                             
                            (%(daysLeft)s days left)
                             
                            (Closed on %(closed_date)s)
                             
                        Due %(days_late)s days ago (%(due_date)s)
                     
                    %(due_date)s
                    (%(days_left)s days left)
                     
                    (%(daysLeft)s days late)
                     
                    (%(daysLeft)s days left)
                     
                %(daysLeft)s days late
                 
                %(daysLeft)s days left
                 
                (%(daysLeft)s days late)
                 
                (%(daysLeft)s days left)
                 
                (Closed on %(closed_date)s)
                 
                Closed on %(closed_date)s
                 
            %(counter)s comment has been written on this message.
             
            %(counter)s comments have been written on this message.
             
            (%(daysLeft)s days late)
             
            (%(daysLeft)s days left)
             
            Completed on %(closed_date)s
             
            Project: %(project_name)s
             
        %(account_type)s at %(organization_name)s
     
        %(account_type)s of %(organization_name)s
     
        Completed: %(completedTasks)s of %(tasks)s (%(completed)s%%)
         
        Completed: %(completedTasks)s out of %(tasks)s (%(completed)s%%)
         
        On %(milestone_name)s of %(project_name)s
         
    %(name)s:
     
    Are you sure you want to delete the member "%(member)s", and his/her user account?
     
    Are you sure you want to delete the milestone "%(milestone)s" ? (It will not delete the assigned task lists)
     
    Are you sure you want to delete the project "%(project)s", along with all its tasks lists, tasks and milestones?
     
    Are you sure you want to delete the task "%(task)s" ?
     
    Are you sure you want to delete the task list "%(task_list)s" ?
     
    Are you sure you want to delete your comment?
     
    Are you sure you want to delete your message "%(message)s", along with all its attachments and comments?
     
    We're glad to see you, %(fullName)s (%(username)s). Are you ready to get some work done?
     400 Bad Request 403 Not Authorized 404 Not Found 500 Internal Server Error A closed project you are assigned to has been reopened. A closed task you are assigned to has been reopened. A comment has been posted in a project message you follow. A comment has been posted in an organization message you follow. A comment has been posted. A member has left us. A message has been deleted. A message has been posted on a project you are working on. A message has been posted. A message has been published in your organization tab. A message has been published. A message has been sticked. A milestone has been added to a project you are working on. A milestone has been closed in a project you are working on. A milestone has been deleted. A milestone has been removed from a project you are working on. A milestone has been reopened in a project you are working on. A project has been deleted. A project you were assigned to has been completed: congratulations! A project you were assigned to has been removed. A task has been added. A task has been deleted. A task list has been assigned to this milestone. A task list has been deleted. A task you are assigned to has been updated. A task you were assigned to has been closed. A task you were assigned to has been deleted. A user with that username already exists. About Account type Active projects Add Add a member Add a milestone Add a project Add a task Add a task list Address Allow comments? Answers Attachment Attachments Authentication error Authentication informations Author Avatar By Can create a project Can manage the organization members Can stick a message in Organization tab Can stick a message in Project Closed Closed on Comment Comments Comments have been disabled for this message. Completed Completed projects Contact information Contact informations Copy Copy task list Date Date of last answer Days left Delete Delete "%(task_name)s" Delete my comment Demo organization Description Description of the organization Description of the project Description of the task Destination project Details Due date Edit Edit "%(task_name)s" Edit informations Edit my comment Edit my profile Edit profile Email Email address Enter a valid username. This value may contain only letters, numbers and @/./+/-/_ characters. Enter the same password as above, for verification. Fax number Feel free to change this description! Feel free to check task lists and help on tasks you're not pinned to. Full name History Home ID of concerned object If checked, all done tasks will be reopened in the copy. In the next 30 days Keep milestone (if possible) Keep pinned members (if possible) Last answer Leave blank if unchanged. Login Logout Made with ❤ by Manager (can create, edit and delete all projects) Mark as completed Member profile updated. Members Members must first be registered in "Organization" tab Message Messages Milestone Milestone creation. Milestone has been completed. Congratulations! Milestone has been reopened. Milestone informations have been updated. Milestones Move Move task list My PMs My organization Name Name of the organization New task list informations No actions registered yet. No active projects. No attachments No completed projects. No information No members. No messages have been posted yet. No task list. No tasks. No upcoming milestones. Nobody Nothing! Notifications Open Organization Organization administrator (all rights) Organization informations have been updated. Organization picture Organization: Organizations Original object name Original project name Overview PMs Participants Participants: Password Password confirmation Phone number Picture Pinned members Please try again later, then contact Projectak developers if it still doesn't work. Please welcome a new member! Post a message Posted by Priority Private messages Project Project concerned Project creation. Project has been closed. Project has been reopened. Project informations have been updated. Project manager Project members Project: Projectak, a project management platform for small organizations Projectak: management tools for non-profit organizations Projects Quick description Read by Read? Recent activity Recent messages Register now! Registration Regular account Reopen Required. 30 characters or fewer. Letters, digits and @/./+/-/_ only. Reset tasks due date Reset tasks priority Reset tasks status Send a PM Sent on Start a conversation Sticky message? Subject Submit Task Task List Task Lists Task has been closed. Task has been reopened. Task informations have been updated. Task list Task list creation. Task list has been assigned to a milestone. Task list has been completed. Congratulations! Task list has been removed from this milestone. Task list has been reopened. Task list informations have been updated. Task lists Tasks The given username and password couldn't identify yourself. Please try again. The requested URL was not found. If you think this is a problem, please contact Projectak developers. The server encountered a problem handling your request. Please try again later, then contact Projectak developers if it still doesn't work. The two password fields didn't match. Title Type of account Type of notification Unread MP count Upcoming milestones Use Edit my profile in main menu to update your information. If you want to change your account type, another organization administrator has to do it. View profile View source We will only keep milestone if you haven't changed project destination. We will only keep pinned members that are part of new project. Website Welcome! What should I do today? You are not allowed to access current URL. If you think this is a problem, please contact your organization managers, or Projectak developers if you think this is an error. You are not working anymore on a project. You are not working anymore on a task. You are part of no conversations. You can add managers in "Organization" tab. You can create a free account to use Projectak. Note that if your organization already uses Projectak, you must not use this form. Instead you should ask an administrator to create your account. You cannot answer from here, because you are not on last page. You cannot comment from here, because you are not on last page. You cannot delete your own account. Another organization administrator has to do it. You have been assigned to a new project. You have been assigned to a task. You have been successfully logged out. Thank you for using Projectak! You have been tagged in a project message. You have been tagged in an organization message. You have no account? You must be logged in order to use Projectak. Your message Your organization informations Your personal informations answers at first name last name name on username Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2016-11-01 18:04+0000
PO-Revision-Date: 2016-11-01 18:06+0000
Last-Translator: b'  <admin@admin.admin>'
Language-Team: LANGUAGE <LL@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: 
Plural-Forms: nplurals=2; plural=(n > 1);
X-Translated-Using: django-rosetta 0.7.12
 
(%(daysLeft)s jours de retard) 
(%(daysLeft)s jours restants) 
(Terminé le %(closed_date)s) 
Date butoir il y a %(days_late)s jours (%(due_date)s) 
%(due_date)s (%(days_left)s jours restants) 
(%(daysLeft)s jours de retard) 
(%(daysLeft)s jours restants) 
%(daysLeft)s jours de retard 
%(daysLeft)s jours restants 
(%(daysLeft)s jours de retard) 
(%(daysLeft)s jours restants) 
(Terminée le %(closed_date)s) 
Terminé le %(closed_date)s 
%(counter)s commentaire a été écrit à propos de ce message. 
%(counter)s commentaires ont été écrits à propos de ce message. 
(%(daysLeft)s jours de retard) 
(%(daysLeft)s jours restants) 
Terminé le %(closed_date)s 
Projet : %(project_name)s 
%(account_type)s chez %(organization_name)s 
%(account_type)s chez %(organization_name)s 
Avancement : %(completedTasks)s sur %(tasks)s (%(completed)s%%) 
Avancement : %(completedTasks)s sur %(tasks)s (%(completed)s%%) 
Dans %(milestone_name)s de %(project_name)s 
%(name)s : 
Êtes-vous sûr(e) de vouloir supprimer le membre "%(member)s" ainsi que son compte utilisateur ? 
Êtes-vous sûr(e) de vouloir supprimer le jalon "%(milestone)s" ? (Cette action n'affectera pas les listes de tâche assignées) 
Êtes-vous sûr(e) de vouloir supprimer le projet "%(project)s", ainsi que toutes ses listes de tâches, tâches, jalons et messages ? 
Êtes-vous sûr(e) de vouloir supprimer la tâche "%(task)s" ? 
Êtes-vous sûr(e) de vouloir supprimer la liste de tâches "%(task_list)s" ? 
Êtes-vous sûr(e) de vouloir supprimer votre commentaire ? 
Êtes-vous sûr(e) de vouloir supprimer le message "%(message)s" ainsi que toutes ses pièces jointes et tous ses commentaires ? 
Nous sommes heureux de vous voir, %(fullName)s (%(username)s). Êtes-vous prêt(e) à accomplir certaines tâches ? 400 Mauvaise requête 403 Accès non autorisé 404 Page non trouvée 500 Erreur serveur Un projet fermé auquel vous êtes assigné(e) a été rouvert. Une tâche fermée à laquelle vous êtes assigné(e) a été rouverte. Un commentaire a été posté dans un message de projet que vous suivez. Un commentaire a été posté dans un message d'organisation que vous suivez. Un commentaire a été posté. Un membre nous a quittés. Un message a été supprimé. Un message a été posté dans un projet sur lequel vous travaillez. Un message a été posté. Un message a été publié dans votre onglet "Organisation". Un message a été publié. Un message a été mis en avant. Un jalon a été ajouté à un projet sur lequel vous travaillez. Un jalon a été complété dans un projet sur lequel vous travaillez. Un jalon a été supprimé. Un jalon a été supprimé du projet sur lequel vous travaillez. Un jalon a été rouvert dans un projet sur lequel vous travaillez. Un projet a été supprimé. Un projet auquel vous êtes assigné(e) a été complété : bravo ! Un projet auquel vous étiez assigné(e) a été supprimé. Une tâche a été ajoutée. Une tâche a été supprimée. Une liste de tâches a été assignée à ce jalon. Une liste de tâches a été supprimée. Une tâche à laquelle vous êtes assigné(e) a été mise à jour. Une tâche sur laquelle vous étiez assigné(e) a été fermée. Une tâche sur laquelle vous étiez assigné(e) a été supprimée. Un compte avec ce nom d'utilisateur existe déjà. À propos de Type de compte Projets actifs Ajouter Ajouter un membre Ajouter un jalon Ajouter un projet Ajouter une tâche Ajouter une liste de tâches Adresse Autoriser les commentaires ? Réponses Pièce jointe Pièces jointes Erreur d'authentification Informations d'authentification Auteur Avatar Par Peut créer un projet Peut gérer les membres de l'organisation Peut mettre en avant un message dans l'onglet "Organisation" Peut mettre en avant un message dans Projet Terminé Terminé le Commentaire Commentaires Les commentaires ont été désactivés pour ce message. Terminé Projets terminés Informations de contact Informations de contact Copier Copier la liste de tâches Date Date de la dernière réponse Jours restants Supprimer Supprimer "%(task_name)s" Supprimer mon commentaire Organisation de démonstration Description Description de l'organisation Description du projet Description de la tâche Projet de destination Informations Date butoir Modifier Modifier "%(task_name)s" Modifier les informations Modifier mon commentaire Modifier mon profil Modifier le profil Email Adresse email Veuillez entrer un nom d'utilisateur valide. Il ne peut contenir que des lettres, chiffres et @/./+/-/_. Veuillez entrer le même mot de passe que précédemment, pour vérification. Numéro de fax N'hésitez pas à changer cette description ! N'hésitez pas à aller vérifier les listes de tâches et aider sur des tâches auxquelles vous n'êtes pas assigné(e). Nom complet Historique Accueil ID de l'objet concerné Si coché, toutes les tâches effectuées seront rouvertes dans la copie. Dans les 30 prochains jours Conserver le jalon (si possible) Garder les assignations de membres (si possible) Dernière réponse Laisser vide si inchangé Connexion Déconnexion Réalisé avec ❤ par Responsable (peut créer, édit et supprimer tous les projets) Marquer comme terminé Le profil du membre a été mis à jour. Membres Les membres doivent d'abord être enregistrés dans l'onglet "Organisation" Message Messages Jalon Création d'un jalon. Le jalon a été validé. Bravo ! Le jalon a été rouvert. Les informations du jalon ont été mises à jour. Jalons Déplacer Déplacer la liste de tâches Mes messages privés Mon organisation Nom Nom de l'organisation Nouvelles informations de la liste de tâches Aucune entrée. Aucun projet actif. Aucune pièce jointe Aucun projet terminé. Pas d'information Aucun membre. Aucun message n'a été posté. Aucune liste de tâches. Aucune tâche. Aucun jalon à venir. Personne Rien ! Notifications Ouvert Organisation Administrateur (tous les droits) Les informations de l'organisation ont été mises à jour. Image de l'organisation Organisation : Organisations Nom original de l'objet Nom original du projet Vue d'ensemble Messages privés Participants Participants : Mot de passe Confirmation de mot de passe Numéro de téléphone Image Membres taggés Merci d'essayer de recharger la page plus tard. Contactez les développeurs de Projectak si cela ne fonctionne toujours pas. Merci d'accueillir un nouveau membre ! Poster un message Posté par Priorité Messages privés Projet Projet concerné Création du projet. Le projet a été fermé. Le projet a été rouvert. Les informations du projet ont été mises à jour. Responsable de projet Membres du projet Projet : Projectak, une plateforme de gestion de projets pour petites organisations Projectak : outils de gestion pour organisations à but non lucratif Projets Description rapide Lu par Lu ? Activité récente Messages récents Inscrivez-vous maintenant ! Inscription Compte standard Rouvrir Requis. 30 caractères ou moins. Lettres, chiffres et @/./+/-/_ uniquement. Réinitialiser les dates butoir des tâches Réinitialiser les priorités des tâches Réinitialiser le statut des tâches Envoyer un MP Envoyé le Démarrer une conversation Message mis en avant ? Sujet Envoyer Tâche Liste de tâche Listes de tâches La tâche a été complétée. La tâche a été rouverte. Les informations de la tâche ont été mises à jour. Liste de tâches Création de la liste de tâches. La liste de tâches a été assignée à un jalon. La liste de tâches a été complétée. Bravo ! La liste de tâches a été enlevée de ce jalon. La liste de tâches a été rouverte. Les informations de la liste de tâches ont été mises à jour. Listes de tâches Tâches Le nom d'utilisateur et le mot de passe donnés n'ont pas permis de vous identifier. Merci d'essayer à nouveau. La page demandée n'a pas été trouvée. Si vous pensez qu'il s'agit d'un problème, merci de contacter les développeurs de Projectak. Le serveur a rencontré un problème en traitant votre requête. Merci d'essayer de recharger la page plus tard. Contactez les développeurs de Projectak si cela ne fonctionne toujours pas. Les deux mots de passe ne correspondent pas. Titre Type de compte Type de la notification Nombre de MP non lus Jalons à venir Utilisez "Modifier mon profil" dans le menu principal pour modifier vos informations. Si vous voulez changer votre type de compte, un autre administrateur doit le faire. Voir le profil Voir la source Nous ne conserverons le jalon que si vous n'avez pas changé le projet de destination. Nous ne conserverons les assignations de membres qui font partie du projet de destination. Site Internet Bienvenue ! Que devrais-je faire aujourd'hui ? Vous n'êtes pas autorisé à accéder à cette page. Si vous pensez que c'est un problème, merci de contacter les responsables de votre organisation, ou les développeurs de Projectak si vous pensez que c'est une erreur. Vous ne travaillez désormais plus sur un projet. Vous ne travaillez désormais plus sur une tâche. Vous ne faites partie d'aucune conversation Vous pouvez ajouter des responsables dans l'onglet "Organisation". Vous pouvez créer un compte gratuit pour utiliser Projectak. Veuillez noter que si votre organisation utilise déjà Projectak, vous ne devez pas utiliser ce formulaire. Vous devriez plutôt demander à un administrateur de vous créer un compte. Vous ne pouvez pas répondre d'ici, car vos n'êtes pas sur la dernière page. Vous ne pouvez pas commenter d'ici, car vous n'êtes pas sur la dernière page. Vous ne pouvez pas supprimer votre propre compte. Un autre administrateur doit le faire. Vous avez été assigné(e) à un nouveau projet. Vous avez été assigné(e) à une tâche. Vous vous êtes déconnecté(e) avec succès. Merci d'utiliser Projectak ! Vous avez été taggé(e) dans un message de projet. Vous avez été taggé(e) dans un message d'organisation. Vous n'avez pas de compte ? Vous devez être connecté(e) afin d'utiliser Projectak. Votre message Les informations de votre organisation Vos informations personnelles réponses à Prénom Nom nom le nom d'utilisateur 