from functools import wraps
from django.utils.decorators import available_attrs
from django.core.exceptions import PermissionDenied
from permission.decorators.utils import redirect_to_login


__all__ = ['permission_required']

# applyquer seulement sur les classes
def permission_required(perm, queryset=None, login_url=None, raise_exception=True):
    def wrapper(cls):
        def view_wrapper(view_func):
            @wraps(view_func, assigned=available_attrs(view_func))
            def inner(self, request, *args, **kwargs):
                if perm.startswith('organization.') and perm.endswith('_organization'):
                    obj = request.user.contact.organization
                elif perm.startswith('project.') and perm.endswith('_project'):
                    obj = self.get_project()
                else:
                    obj = get_object_from_cls_instance(self, queryset, request, *args, **kwargs)
                #else:
                #    raise Exception('Invalid permission: look in projectak/decorators/permission_required.py.')

                if not request.user.has_perm(perm, obj=obj):
                    if raise_exception:
                        raise PermissionDenied
                    else:
                        return redirect_to_login(request, login_url)

                return view_func(self, request, *args, **kwargs)
            return inner
        cls.dispatch = view_wrapper(cls.dispatch)
        return cls
    return wrapper

def get_object_from_cls_instance(instance, queryset, request, *args, **kwargs):
    from django.views.generic.edit import BaseCreateView

    instance.request = request
    instance.args = args
    instance.kwargs = kwargs

    if hasattr(instance, 'get_queryset') and not queryset:
        queryset = instance.get_queryset()
    elif hasattr(instance, 'queryset') and not queryset:
        queryset = instance.queryset
    elif hasattr(instance, 'model') and not queryset:
        queryset = instance.model._default_manager.all()

    if hasattr(instance, 'get_object'):
        try:
            obj = instance.get_object(queryset)
        except AttributeError as e:
            if isinstance(instance, BaseCreateView):
                obj = None
            else:
                raise e
    elif hasattr(instance, 'object'):
        obj = instance.object
    else:
        obj = None
    return obj
