from django.conf.urls import url, include

from . import views
from .views import MemberEditProfileView

urlpatterns = [
    url(r'^$', views.homepage, name='homepage'),
    url(r'^register/$', views.register, name='register'),
    url(r'^profile/edit/$', MemberEditProfileView.as_view(), name='edit_profile'),
]
