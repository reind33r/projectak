from django.core.management.base import BaseCommand, CommandError

from organization.models import Organization, Contact
from django.utils.translation import ugettext_lazy as _

class Command(BaseCommand):
    help = 'Create a demo account'

    def handle(self, *args, **options):
        from django.contrib.auth.models import User
        user = User.objects.create_user(username='demo', password='demo', email='john.doe@noemail.void', first_name='John', last_name='Doe')
        user.save()

        organization = Organization(name=_("Demo organization"), description=_("Feel free to change this description!"))
        organization.save()

        contact = Contact(user=user, account_type="admin", organization=organization)
        contact.save()

        organization.manager = contact
        organization.save()

        self.stdout.write(self.style.SUCCESS('Successfully created demo account'))
