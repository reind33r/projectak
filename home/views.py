from django.shortcuts import render, redirect
from django.contrib.auth import login

from django.views.generic.edit import UpdateView

from .forms import ContactRegisterForm
from organization.forms import OrganizationForm

def homepage(request):
    return render(request, 'home/homepage.html')

def register(request):
    if request.method == 'POST':
        formContact = ContactRegisterForm(data=request.POST, prefix="contact")
        formOrganization = OrganizationForm(data=request.POST, prefix="organization")

        if formContact.is_valid() and formOrganization.is_valid():
            organization = formOrganization.save()

            contact = formContact.save(commit=False)

            contact.account_type = "admin"
            contact.organization = organization

            user = contact.user
            user.save()
            contact.user = user
            contact.save()

            organization.manager = contact
            organization.save()

            login(request, user, backend='django.contrib.auth.backends.ModelBackend')
            return redirect('homepage')

    else:
        formContact = ContactRegisterForm(prefix="contact")
        formOrganization = OrganizationForm(prefix="organization")

    return render(request, 'home/register.html', {
        'formContact': formContact,
        'formOrganization': formOrganization,
    })


class MemberEditProfileView(UpdateView):
    from organization.models import Contact
    model = Contact
    from .forms import ContactEditForm
    form_class = ContactEditForm

    def get_object(self):
        return self.request.user.contact

    def form_valid(self, form):
        form.set_organization(self.request.user.contact.organization)
        member = form.save()
        return super(MemberEditProfileView, self).form_valid(form)

    template_name = "home/edit_profile.html"
    context_object_name = "member"
