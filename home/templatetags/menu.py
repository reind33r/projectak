from django import template

from django.http.request import HttpRequest

register = template.Library()

@register.simple_tag
def active(request, pattern):
    if isinstance(request, HttpRequest):
        import re
        if re.search(pattern, request.path):
            return 'active'

    return ''