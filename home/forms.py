from django import forms
from material import Layout, Row, Column, Fieldset, Span4, Span8
from django.utils.translation import ugettext_lazy as _

from organization.forms import ContactForm, ContactAddForm

class ContactRegisterForm(ContactAddForm):
    account_type = None

    class Meta(ContactAddForm.Meta):
        fields = ('phone', 'address')

    layout = Layout(
        Fieldset(_('Authentication informations'),
            Row('user_username'),
            Row('user_password1', 'user_password2'),
        ),
        Fieldset(_('Contact informations'),
            Row('user_first_name', 'user_last_name'),
            'address',
            Row('user_email', 'phone'),
        ),
    )

class ContactEditForm(ContactForm):
    account_type = None

    class Meta(ContactForm.Meta):
        fields = ('phone', 'address', 'avatar', 'color')

    layout = Layout(
        Row('avatar', 'color'),
        Fieldset(
            _('Contact informations'),
            Row('user_first_name', 'user_last_name'),
            'address',
            Row('user_email', 'phone'),
        ),
        Fieldset(_('Authentication informations'),
            Row('user_password1', 'user_password2'),
        ),
    )
